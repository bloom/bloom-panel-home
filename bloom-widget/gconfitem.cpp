/* * This file is part of libgq *
 *
 * Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved.
 *
 * Contact: Marius Vollmer <marius.vollmer@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QVariant>
#include <QtDebug>
#include <QPoint>
#include <QSize>

#include "gconfitem.h"

#define GCONF_ENABLE_INTERNALS
#include <gconf/gconf-client.h>

#include <glib.h>
#include <gconf/gconf-value.h>

// GConfEngine *engine = NULL;
// GConfClient *client = NULL;

struct GConfItemPrivate {
    QString key;
    QVariant value;
    guint notify_id;

    static void notify_trampoline(GConfClient*, guint, GConfEntry *, gpointer);
};

#define withClient(c) for(GConfClient *c = (g_type_init(), gconf_client_get_default()); c; g_object_unref(c), c=NULL)
// #define withClient(c);

static QByteArray convertKey (QString key)
{
    if (key.startsWith('/'))
        return key.toUtf8();
    else
    {
        qWarning() << "Using dot-separated key names with GConfItem is deprecated." << key;
        qWarning() << "Please use" << '/' + key.replace('.', '/') << "instead of";
        return '/' + key.replace('.', '/').toUtf8();
    }
}

static QString convertKey(const char *key)
{
    return QString::fromUtf8(key);
}

static QVariant convertValue(GConfValue *src)
{
    if (!src) {
        return QVariant();
    } else {
        switch (src->type) {
        case GCONF_VALUE_INVALID:
            return QVariant(QVariant::Invalid);
        case GCONF_VALUE_BOOL:
            return QVariant((bool)gconf_value_get_bool(src));
        case GCONF_VALUE_INT:
            return QVariant(gconf_value_get_int(src));
        case GCONF_VALUE_FLOAT:
            return QVariant(gconf_value_get_float(src));
        case GCONF_VALUE_STRING:
            return QVariant(QString::fromUtf8(gconf_value_get_string(src)));
        case GCONF_VALUE_LIST:
            switch (gconf_value_get_list_type(src)) {
            case GCONF_VALUE_STRING:
                {
                    QStringList result;
                    for (GSList *elts = gconf_value_get_list(src); elts; elts = elts->next)
                        result.append(QString::fromUtf8(gconf_value_get_string((GConfValue *)elts->data)));
                    return QVariant(result);
                }
            default:
                {
                    QList<QVariant> result;
                    for (GSList *elts = gconf_value_get_list(src); elts; elts = elts->next)
                        result.append(convertValue((GConfValue *)elts->data));
                    return QVariant(result);
                }
            }
        case GCONF_VALUE_SCHEMA:
        default:
            return QString("schema");
        }
    }
}

static GConfValue *convertString(const QString &str)
{
    GConfValue *v = gconf_value_new (GCONF_VALUE_STRING);
    gconf_value_set_string (v, str.toUtf8().data());
    return v;
}

static GConfValueType primitiveType (const QVariant &elt)
{
    switch(elt.type()) {
    case QVariant::String:
        return GCONF_VALUE_STRING;
    case QVariant::Int:
        return GCONF_VALUE_INT;
    case QVariant::Double:
        return GCONF_VALUE_FLOAT;
    case QVariant::Bool:
        return GCONF_VALUE_BOOL;
    default:
        return GCONF_VALUE_INVALID;
    }
}

static GConfValueType uniformType(const QList<QVariant> &list)
{
    GConfValueType result = GCONF_VALUE_INVALID;

    foreach (const QVariant &elt, list) {
        GConfValueType elt_type = primitiveType (elt);

        if (elt_type == GCONF_VALUE_INVALID)
            return GCONF_VALUE_INVALID;

        if (result == GCONF_VALUE_INVALID)
            result = elt_type;
        else if (result != elt_type)
            return GCONF_VALUE_INVALID;
    }

    if (result == GCONF_VALUE_INVALID)
        return GCONF_VALUE_STRING;  // empty list.
    else
        return result;
}

static int convertValue(const QVariant &src, GConfValue **valp, QVariant *dst = NULL)
{
    GConfValue *v;

    switch(src.type()) {
    case QVariant::Invalid:
        v = NULL;
        if(dst) *dst = src;
        break;
    case QVariant::Bool:
        v = gconf_value_new (GCONF_VALUE_BOOL);
        gconf_value_set_bool (v, src.toBool());
        if(dst) *dst = src;
        break;
    case QVariant::Int:
        v = gconf_value_new (GCONF_VALUE_INT);
        gconf_value_set_int (v, src.toInt());
        if(dst) *dst = src;
        break;
    case QVariant::Double:
        v = gconf_value_new (GCONF_VALUE_FLOAT);
        gconf_value_set_float (v, src.toDouble());
        if(dst) *dst = src;
        break;
    case QVariant::String:
        v = convertString(src.toString());
        if(dst) *dst = src;
        break;
    case QVariant::StringList:
        {
            GSList *elts = NULL;
            v = gconf_value_new(GCONF_VALUE_LIST);
            gconf_value_set_list_type(v, GCONF_VALUE_STRING);
            foreach (const QString &str, src.toStringList())
                elts = g_slist_prepend(elts, convertString(str));
            gconf_value_set_list_nocopy(v, g_slist_reverse(elts));
            if(dst) *dst = src;
            break;
        }
    case QVariant::Point:
        {
            GSList *elts = NULL;
            GConfValue *x = gconf_value_new (GCONF_VALUE_INT);
            GConfValue *y = gconf_value_new (GCONF_VALUE_INT);
            gconf_value_set_int (x, src.toPoint().x());
            gconf_value_set_int (y, src.toPoint().y());
            v = gconf_value_new(GCONF_VALUE_LIST);
            gconf_value_set_list_type(v, GCONF_VALUE_INT);
            elts = g_slist_prepend(elts, x);
            elts = g_slist_prepend(elts, y);
            gconf_value_set_list_nocopy(v, g_slist_reverse(elts));
            if(dst) *dst = QList<QVariant>() << src.toPoint().x() << src.toPoint().y();
            break;
        }
    case QVariant::Size:
        {
            GSList *elts = NULL;
            GConfValue *width = gconf_value_new (GCONF_VALUE_INT);
            GConfValue *height = gconf_value_new (GCONF_VALUE_INT);
            gconf_value_set_int(width, src.toSize().width());
            gconf_value_set_int(height, src.toSize().height());
            v = gconf_value_new(GCONF_VALUE_LIST);
            gconf_value_set_list_type(v, GCONF_VALUE_INT);
            elts = g_slist_prepend(elts, width);
            elts = g_slist_prepend(elts, height);
            gconf_value_set_list_nocopy(v, g_slist_reverse(elts));
            if(dst) *dst = QList<QVariant>() << src.toSize().width() << src.toSize().height();
            break;
        }
    case QVariant::List:
        {
            GConfValueType elt_type = uniformType(src.toList());
            if (elt_type == GCONF_VALUE_INVALID)
                v = NULL;
            else
            {
                GSList *elts = NULL;
                v = gconf_value_new(GCONF_VALUE_LIST);
                gconf_value_set_list_type(v, elt_type);
                foreach (const QVariant &elt, src.toList())
                {
                    GConfValue *val = NULL;
                    convertValue(elt, &val);  // guaranteed to succeed.
                    elts = g_slist_prepend(elts, val);
                }
                gconf_value_set_list_nocopy(v, g_slist_reverse(elts));
            }
            if(dst) *dst = src;
            break;
        }
    default:
        return 0;
    }

    *valp = v;
    return 1;
}

void GConfItemPrivate::notify_trampoline (GConfClient*,
                                             guint,
                                             GConfEntry *entry,
                                             gpointer data)
{
    GConfItem *item = (GConfItem *)data;


    item->update_value (true, entry->key, convertValue(entry->value));
}

void GConfItem::update_value (bool emit_signal, const QString& key, const QVariant& value)
{
    QVariant new_value;

    if (emit_signal) {
        emit subtreeChanged(key, value);
    }

    withClient(client) {
        GError *error = NULL;
        QByteArray k = convertKey(priv->key);
        GConfValue *v = gconf_client_get(client, k.data(), &error);

        if (error) {
            qWarning() << error->message;
            g_error_free (error);
            new_value = priv->value;
        } else {
            new_value = convertValue(v);
            if (v)
                gconf_value_free(v);
        }
    }

    if (new_value != priv->value) {
        priv->value = new_value;
        if (emit_signal)
            emit valueChanged();
    }
}

QString GConfItem::key() const
{
    return priv->key;
}

QVariant GConfItem::value() const
{
    return priv->value;
}

GConfItem::operator float() const
{
    return value().toFloat();
}

GConfItem::operator double() const
{
    return value().toDouble();
}

GConfItem::operator int() const
{
    return value().toInt();
}

GConfItem::operator bool() const
{
    return value().toBool();
}

GConfItem::operator QString() const
{
    return value().toString();
}

GConfItem::operator QPoint() const
{
    QList<QVariant> values = value().toList();
    if(values.size() >= 2) {
        return QPoint(value().toList()[0].toInt(), value().toList()[1].toInt());
    }
    else {
        return QPoint(0, 0);
    }
}

GConfItem::operator QSize() const
{
    QList<QVariant> values = value().toList();
    if(values.size() >= 2) {
        return QSize(value().toList()[0].toInt(), value().toList()[1].toInt());
    }
    else {
        return QSize(0, 0);
    }
}

void GConfItem::set(const QVariant &val, bool emit_signal)
{
    withClient(client) {
        QByteArray k = convertKey(priv->key);
        GConfValue *v;
        QVariant new_val;
        if (convertValue(val, &v, &new_val)) {
            GError *error = NULL;

            if (v) {
                gconf_client_set(client, k.data(), v, &error);
                gconf_value_free(v);
            } else {
                gconf_client_recursive_unset(client, k.data(), GCONF_UNSET_INCLUDING_SCHEMA_NAMES, &error);
                gconf_client_unset(client, k.data(), &error);
                gconf_client_clear_cache (client);
            }

            if (error) {
                qWarning() << error->message;
                g_error_free(error);
            } else if (priv->value != new_val) {
                priv->value = new_val;
                if (emit_signal)
                    emit valueChanged();
            }

        } else
            qWarning() << "Can't store a" << val.typeName();
    }
}

GConfItem& GConfItem::operator =(float val)
{
    set(QVariant(double(val)), false);
    return *this;
}

GConfItem& GConfItem::operator =(double val)
{
    set(QVariant(val), false);
    return *this;
}

GConfItem& GConfItem::operator =(int val)
{
    set(QVariant(val), false);
    return *this;
}

GConfItem& GConfItem::operator =(bool val)
{
    set(QVariant(val), false);
    return *this;
}

GConfItem& GConfItem::operator =(const char *val)
{
    set(QVariant(val), false);
    return *this;
}

GConfItem& GConfItem::operator =(QString val)
{
    set(QVariant(val), false);
    return *this;
}

GConfItem& GConfItem::operator =(QPoint val)
{
    set(QList<QVariant>() << val.x() << val.y(), false);
    return *this;
}

GConfItem& GConfItem::operator =(QSize val)
{
    set(QList<QVariant>() << val.width() << val.height(), false);
    return *this;
}

void GConfItem::unset() {
    set(QVariant());
}

bool GConfItem::dirExists()
{
    withClient(client) {
        return gconf_client_dir_exists(client, convertKey(priv->key).data(), NULL);
    }
    return false;
}

QList<QString> GConfItem::listDirs() const
{
    QList<QString> children;

    withClient(client) {
        QByteArray k = convertKey(priv->key);
        GSList *dirs = gconf_client_all_dirs(client, k.data(), NULL);
        for (GSList *d = dirs; d; d = d->next) {
            children.append(convertKey((char *)d->data));
            g_free (d->data);
        }
        g_slist_free (dirs);
    }

    return children;
}

QList<QString> GConfItem::listEntries() const
{
    QList<QString> children;

    withClient(client) {
        QByteArray k = convertKey(priv->key);
        GSList *entries = gconf_client_all_entries(client, k.data(), NULL);
        for (GSList *e = entries; e; e = e->next) {
            children.append(convertKey(((GConfEntry *)e->data)->key));
            gconf_entry_free ((GConfEntry *)e->data);
        }
        g_slist_free (entries);
    }

    return children;
}

GConfItem::GConfItem(const QString &key, const QString &schema, QObject *parent)
    : QObject (parent)
{
    bool set_schema = true;
    priv = new GConfItemPrivate;
    priv->key = key;
    withClient(client) {
        if (!schema.isEmpty()) {
             GConfEntry *entry = gconf_engine_get_entry (client->engine, convertKey(priv->key).data(), NULL, TRUE, NULL);
             if (entry) {
                 if (gconf_entry_get_schema_name (entry)) {
                     set_schema = false;
                 }
                 gconf_entry_free (entry);
             }
             if (set_schema) {
                 gconf_engine_associate_schema (client->engine, convertKey(priv->key).data(), convertKey(schema).data(), NULL);
                 gconf_client_clear_cache (client);
             }
        }
        update_value (false, "", QVariant());
        QByteArray k = convertKey(priv->key);
        gconf_client_add_dir (client, k.data(), GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
        priv->notify_id = gconf_client_notify_add (client, k.data(),
                                                   GConfItemPrivate::notify_trampoline, this,
                                                   NULL, NULL);
    }
}

GConfItem::~GConfItem()
{
    withClient(client) {
        QByteArray k = convertKey(priv->key);
        gconf_client_notify_remove (client, priv->notify_id);
        gconf_client_remove_dir (client, k.data(), NULL);
    }
    delete priv;
}
