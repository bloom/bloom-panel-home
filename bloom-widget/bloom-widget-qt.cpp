#include "bloom-widget-qt.h"

#include <QDBusConnection>
#include <QDBusConnectionInterface>

QString get_free_service_name(QString service)
{
    QDBusConnectionInterface *dbus_interface = QDBusConnection::sessionBus().interface();
    QString service_name;
    int i = 0;
    do {
         service_name = QString(service + "%1").arg(++i);
    } while (dbus_interface->isServiceRegistered(service_name));
    return service_name;
}

