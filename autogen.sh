#!/bin/sh
[ -e config.cache ] && rm -f config.cache

libtoolize --automake
intltoolize --copy --automake --force
aclocal
autoconf
autoheader
automake -a
./configure $@
exit

