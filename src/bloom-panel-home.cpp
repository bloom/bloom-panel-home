/*
 * bloom-panel-home
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: Josselin ROGER
 *         Sylvain BAUBEAU
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bloom-panel-home.h"
#include "bloom-widget-container.h"

#include <gconf/gconf-client.h>
#include <QVBoxLayout>

#define DEFAULT_GROUP_NAME "Desktop Entry"

#include <locale.h>
#include <libintl.h>

#define _(STRING)    gettext(STRING)

class BloomWidgetPendingCallWatcher : public QDBusPendingCallWatcher
{
public:
    QString type;
    QString key;
    bool embed;

    BloomWidgetPendingCallWatcher(const QDBusPendingCall &call, QObject *parent,
                                  QString type, QString key, bool embed) :
        QDBusPendingCallWatcher(call, parent) {
        this->type = type;
        this->key = key;
        this->embed = embed;
    }
};

BloomPanelHome::BloomPanelHome(QWidget *parent) :
    QWidget(parent, Qt::FramelessWindowHint),
    dbus_watcher(this),
    gconf_entry(WIDGETS_GCONF_KEY)
{
    panel = NULL;
    loaded = false;

    QFile file(THEMEDIR"/default/bloom-panel-home.css");
    QString theme = "";
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("File can't be found or opened");
    } else {
        QTextStream stream ( &file );
        while( !stream.atEnd() ) {
            theme += stream.readLine();
        }
    }
    file.close();
    theme.replace("THEMEDIR", THEMEDIR);
    setStyleSheet(theme);

    /* create the desktop */
    desktop = new BloomPanelDesktop(this);
    vlayout = new QVBoxLayout();
    vlayout->setMargin(0);
    vlayout->addWidget(desktop);
    setLayout(vlayout);
    desktop->show();

    connect(&gconf_entry, SIGNAL(subtreeChanged(const QString &, const QVariant &)), this, SLOT(widget_list_changed(const QString &, const QVariant &)));

    dbus_watcher.setConnection(QDBusConnection::sessionBus());
    connect(&dbus_watcher, SIGNAL(serviceRegistered(const QString &)), this, SLOT(service_started(const QString &)));
}

BloomPanelHome::~BloomPanelHome()
{
}

void BloomPanelHome::changeEvent(QEvent* event)
{
#ifndef NO_BLOOM_PANEL
    if(panel && event->type() == QEvent::ActivationChange) {
        if (!isActiveWindow())
            mpl_panel_client_request_hide(panel);
        else if (isActiveWindow())
            mpl_panel_client_request_show(panel);
    }
#endif
}

void BloomPanelHome::mousePressEvent(QMouseEvent* event)
{
#ifndef NO_BLOOM_PANEL
    if(panel && event->type() == QEvent::MouseButtonPress) {
        mpl_panel_client_request_show(panel);
    }
#endif
}

void BloomPanelHome::load_widgets()
{
    if (loaded)
        return;

    GError *error = NULL;
    GConfClient *client = gconf_client_get_default();
    GSList *entries = gconf_client_all_dirs(client, WIDGETS_GCONF_KEY, &error);
    for (GSList *e = entries; e; e = e->next) {
        GConfEntry *entry = (GConfEntry*) &e->data;
        QString key = gconf_entry_get_key(entry);
        QString type_entry = QString(key) + "/deleted";
        error = NULL;
        if (!gconf_client_get_bool(client, type_entry.toStdString().c_str(), &error)) {
            qDebug() << "Loading widget from" << key;

            // Get the D-Bus service name for the widget
            QString type_entry = QString(key) + "/type";
            const char *type = gconf_client_get_string(client, type_entry.toStdString().c_str(), &error);

            request_widget(type, key);
        }
    }
    loaded = true;
}

void BloomPanelHome::drop_new_widget(const QString &interface, const QString &schema)
{
    QString gconf_entry = get_free_gconf_entry();
    GConfItem gconf_schema(gconf_entry + "/schema");
    gconf_schema = schema;
    qDebug() << "Dropping new widget of type" << interface;
    BloomWidgetContainer *new_widget = request_widget(interface,
                                                      gconf_entry);
    if (new_widget == NULL) {
        QMessageBox dlg (QMessageBox::Information, _("Not enough space for this widget"),
                                                   _("There is not enough space on workspace to place this widget.\n"
                                                     "Please remove some widgets from your workspace."));
        dlg.exec();
        return;
    }

    new_widget->move(QCursor::pos());
    desktop->selected_widget = new_widget;
    desktop->widgets.append(new_widget);
    desktop->position_on_widget = QPoint(20, 10);
    desktop->highlighted_rect = new_widget->rect();
    desktop->highlighted_widget->setGeometry(desktop->highlighted_rect);
    new_widget->show();
    new_widget->raise();
    new_widget->setFocus(Qt::MouseFocusReason);
    desktop->is_out = false;
    desktop->grabMouse();
}

QString BloomPanelHome::get_free_gconf_entry()
{
    QString entry;
    int i = 0;
    do {
        entry = entry.sprintf("%s/%d", WIDGETS_GCONF_KEY, i++);
    } while(GConfItem(entry).dirExists());
    return entry;
}

BloomWidgetContainer* BloomPanelHome::request_widget(QString interface, QString key, bool embed)
{
    if (interface.isEmpty()) {
        qDebug() << "Empty service name for entry" << key;
        return NULL;
    }
    int count = 1;
    for (int i = 0; i < desktop->widgets.size(); i++) {
        BloomWidgetContainer *widget = desktop->widgets[i];
        if (QString(widget->type) == interface)
            count++;
    }
    QString service_name = QString(interface + "%1").arg(count);
    BloomWidgetContainer *widget = create_container_for_widget(service_name, interface, "/Widget", key, embed);
    if (desktop->add_widget(widget) != NULL) {
        dbus_watcher.addWatchedService(service_name);
        spawn_widget(interface);
        return widget;
    }
    else {
        return NULL;
    }
}

QString get_dbus_service_executable(QString service)
{
    QDir services_dir(DATADIR "/bloom/widgets");
    QStringList services = services_dir.entryList(QStringList("*.desktop"));

    for (int i = 0; i < services.size(); i++) {
        GError *error = NULL;
        QString filename = services_dir.filePath(services[i]);
        GKeyFile *key_file = g_key_file_new();
        g_key_file_load_from_file(key_file, filename.toLatin1(), G_KEY_FILE_NONE, &error);
        gchar *name = g_key_file_get_string(key_file, DEFAULT_GROUP_NAME, "X-Bloom-Service", &error);
        QString q_exec;
        if (service == name) {
            gchar *exec = g_key_file_get_string(key_file, DEFAULT_GROUP_NAME, "Exec", &error);
            q_exec = exec;
            g_free(exec);
        }
        g_free(name);
        g_key_file_free(key_file);
        if (!q_exec.isEmpty())
            return q_exec;
    }
    return QString();
}

void BloomPanelHome::service_started(const QString &service)
{
    for (int i = desktop->widgets.size() - 1; i >= 0; i--) {
        if (desktop->widgets[i]->get_service_name() == service) {
            desktop->widgets[i]->init_embedded();
            break;
        }
    }
}

void BloomPanelHome::spawn_widget(QString interface)
{
    QProcess *process = new QProcess();
    QStringList cmd_line = get_dbus_service_executable(interface).split(" ");
    process->start(cmd_line[0], cmd_line.mid (1));
}

void BloomPanelHome::widget_list_changed(const QString& key, const QVariant& value)
{
    /*
    for (int i = 0; i < desktop->widgets.size(); i++) {
        if (desktop->widgets[i]->get_gconf_entry().key() == key and value.isNull()) {
            desktop->widgets[i]->close();
        }
    }
    */
}

BloomWidgetContainer* BloomPanelHome::create_container_for_widget(QString service, QString type, QString path, QString key, bool embed)
{
    return new BloomWidgetContainer(service, type, path,
                                    QDBusConnection::sessionBus(), desktop,
                                    key, embed);
}

void BloomPanelHome::showEvent(QShowEvent *event)
{
    if (!loaded) {
        load_widgets();
        loaded = true;
    }
}
