#ifndef BLOOM_WIDGET_CONTEXT_MENU_H_
#define BLOOM_WIDGET_CONTEXT_MENU_H_

#include <QtCore>
#include <QtGui>

class BloomWidgetContainer;

class BloomWidgetContextMenu : public QMenu
{
    Q_OBJECT

    public:
        BloomWidgetContextMenu(BloomWidgetContainer *parent);

    protected:
        void mousePressEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);

    public slots:
        void show_menu(QPoint);
        void show_menu();
        void changed_style(QAction*);

    private:
        BloomWidgetContainer *widget;
        bool left_button_pressed;
};

#endif /* BLOOM_WIDGET_CONTEXT_MENU_H_ */
