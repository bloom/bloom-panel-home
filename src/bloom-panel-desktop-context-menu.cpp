/*
 * bloom-panel-desktop-context-menu
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: ROGER Josselin
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "bloom-panel-desktop-context-menu.h"
#include "bloom-panel-desktop.h"


/********************************
 *      Public methodes      *
 ********************************/
BloomPanelDesktopContextMenu::BloomPanelDesktopContextMenu(BloomPanelDesktop *parent) : QMenu(parent)
{
    parent = parent;
    left_button_pressed = false;

    /* Action change background */
    QAction *newAct = new QAction(tr("&Change background"), this);
    newAct->setStatusTip(tr("Change the background"));
    connect(newAct, SIGNAL(triggered()), parent, SLOT(choose_background()));

    addAction(newAct);
}

/********************************
 *      Protected methodes      *
 ********************************/

void BloomPanelDesktopContextMenu::mousePressEvent(QMouseEvent *event)
{
    if (!geometry().contains(QCursor::pos())) {
        /* Transfer to the home_panel's mousePressEvent */
        QMouseEvent eventparent(event->type(),
                                event->pos(), // parent->mapFromGlobal(QCursor::pos()),
                                event->button(),
                                event->buttons(),
                                event->modifiers());
        // parent->mousePressEvent(&eventparent);
        event->ignore();
        return;
    } else if (QApplication::mouseButtons() == Qt::LeftButton) {
        /* Allow allow the execution of menu's actions */
        left_button_pressed = true;
    }
}

void BloomPanelDesktopContextMenu::mouseReleaseEvent(QMouseEvent *event)
{
    if (left_button_pressed) {
        hide();
        left_button_pressed = false;
        if (actionAt(mapFromGlobal(QCursor::pos())) != 0)
            emit actionAt(mapFromGlobal(QCursor::pos()))->trigger();
    }
}


/***************************
 *      Private slots      *
 ***************************/
void BloomPanelDesktopContextMenu::show_menu(QPoint pos)
{
    move(pos);
    show();
}
