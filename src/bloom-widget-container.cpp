/*
 * bloom-widget-context-menu
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: BAUBEAU Sylvain
 *         ROGER Josselin
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../config.h"

#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <locale.h>

#include "bloom-widget-container.h"
#include "bloom-panel-desktop.h"
#include "bloom-widget-context-menu.h"
#include "../bloom-widget/bloom-widget-dbus-proxy.h"
#include "bloom-panel-desktop-grid.h"

#define _(String) gettext (String)

#define WIDGET_CREATION_TIMEOUT 180

QString get_widget_schema(QString name, QString schema_name = "")
{
    QString schema;
    schema = "/schemas/desktop/bloom/widget/" + schema_name + "/" + name;
    if (!schema_name.isEmpty()) {
        GConfItem gconf_item(schema);
        if (!gconf_item.value().isNull())
            return schema;
    }
    return "/schemas/desktop/bloom/widget/" + name;
}

BloomWidgetContainer::BloomWidgetContainer(const QString &_service, const QString &interface, const QString &path,
                                           const QDBusConnection &connection, BloomPanelDesktop *parent,
                                           QString gconf_prefix, bool embed) :
    QWidget(parent),
    on_the_grid(true),
    gconf_entry(gconf_prefix),
    schema(gconf_prefix + "/schema", "/schemas/desktop/bloom/widget/schema"),
    type(gconf_prefix + "/type", get_widget_schema("type", schema)),
    fixed(gconf_prefix + "/fixed", get_widget_schema("fixed", schema)),
    visible(gconf_prefix + "/visible", get_widget_schema("visible", schema)),
    title(gconf_prefix + "/title", get_widget_schema("title", schema)),
    favicon(gconf_prefix + "/icon", get_widget_schema("icon", schema)),
    position_on_grid(gconf_prefix + "/position_on_grid", get_widget_schema("position_on_grid", schema)),
    size_on_grid(gconf_prefix + "/size_on_grid", get_widget_schema("size_on_grid", schema)),
    thickness(gconf_prefix + "/thickness", get_widget_schema("thickness", schema)),
    margin(gconf_prefix + "/margin", get_widget_schema("margin", schema)),
    style(gconf_prefix + "/style", get_widget_schema("style", schema)),
    alpha(gconf_prefix + "/alpha", get_widget_schema("alpha", schema)),
    show_shadow(gconf_prefix + "/shadow", get_widget_schema("shadow", schema)),
    minimum_size_on_grid(gconf_prefix + "/minimum_size_on_grid", get_widget_schema("minimum_size_on_grid", schema)),
    maximum_size_on_grid(gconf_prefix + "/maximum_size_on_grid", get_widget_schema("maximum_size_on_grid", schema)),
    deletable(gconf_prefix + "/deletable", get_widget_schema("deletable", schema)),
    has_options(gconf_prefix + "/options", get_widget_schema("options", schema)),
    auto_hide_caption(gconf_prefix + "/auto_hide_caption", get_widget_schema("auto_hide_caption", schema)),
    deleted(gconf_prefix + "/deleted", get_widget_schema("deleted", schema))
{
    winid = 0;
    service = _service;
    type = interface;
    desktop = parent;
    grid = desktop->get_grid();
    embedded = false;
    shadow = NULL;
    create_call_watcher = NULL;
    on_the_grid = true;
    bool vert = false;
    parent = desktop;
    setObjectName("Bloom_widget");
    allow_manual_resize = true;
    remove_gconf = false;
    position_cursor = 0;
    manual_resize = false;
    background_auto_resize = 0;
    vertical = vert;
    side = "left";
    button_options = NULL;
    button_delete = NULL;
    QFont font("SansSerif", 8);
    font.setStyleStrategy(QFont::PreferQuality);
    
    menu = new BloomWidgetContextMenu(this);
    setContextMenuPolicy(Qt::DefaultContextMenu);

    if (on_the_grid) {
        setMinimumSize(grid->square_to_px(QSize(minimum_size_on_grid)));
        setMaximumSize(grid->square_to_px(QSize(maximum_size_on_grid)));

        if (((QSize) size_on_grid).isEmpty()) {
            ((QSize) size_on_grid).scale(grid->px_to_square(QSize(rand() % 140 + 60,
                                                        rand() % 140 + 60)),
                               Qt::IgnoreAspectRatio);
        }
        resize(grid->square_to_px((QSize) size_on_grid));

        header_left = new QWidget(this);
        header_left->resize(HEADER_CORNERS_WIDTH, HEADER_HEIGHT);
        header_left->setObjectName("header_left");

        header = new QWidget(this);
        header->resize(30, HEADER_HEIGHT);
        header->setObjectName("header");

        header_right = new QWidget(this);
        header_right->resize(HEADER_CORNERS_WIDTH, HEADER_HEIGHT);
        header_right->setObjectName("header_right");

        if (auto_hide_caption) {
            show_caption(false);
        } else {
            show_caption(true);
        }
    } else {
        fixed = true;
        resize(grid->square_to_px(QSize(size_on_grid)));
    }

    favicon_label = new QLabel(header);
    favicon_label->setObjectName("favicon");

    title_label = new QLabel(header);
    title_label->setFont(font);
    title_label->setWordWrap(false);
    title_label->setObjectName("title_label");

    frame = new QFrame(this);
    frame->setAutoFillBackground(true);
    frame->setObjectName("frame");

    background = new QWidget(frame);
    background->setObjectName("background");
    palette = background->palette();

    embed_client = new QX11EmbedContainer(background);

    main_layout = new QVBoxLayout(background);
    main_layout->setSpacing(0);
    main_layout->setMargin(0);

    wait_animation = new QLabel(_("Please wait while the widget is loading"));
    wait_animation->setWordWrap(true);
    wait_animation->setAlignment(Qt::AlignCenter|Qt::AlignHCenter);
    wait_animation->setObjectName("wait_animation");
    QString theme = THEMEDIR;
    theme += "/" + QString(style);
    wait_movie = new QMovie(theme + "/widget-loading.gif");
    wait_animation->setMovie(wait_movie);
    wait_movie->start();
    main_layout->addWidget(wait_animation);

    if (bool(deletable)) {
        if (on_the_grid)
            button_delete = new QPushButton(header);
        else
            button_delete = new QPushButton(frame);
        button_delete->setWindowFlags(Qt::WindowStaysOnTopHint);
        button_delete->setObjectName("deletePushButton");
        button_delete->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        button_delete->hide();
        connect(button_delete, SIGNAL(clicked()), this, SLOT(remove()));
    }

    if (bool(has_options)) {
        if (on_the_grid)
            button_options = new QPushButton(header);
        else
            button_options = new QPushButton(frame);
        button_options->setWindowFlags(Qt::WindowStaysOnTopHint);
        button_options->setObjectName("optionsPushButton");
        button_options->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        button_options->hide();
        connect(button_options, SIGNAL(clicked()), menu, SLOT(show_menu()));
    }

    setAcceptDrops(true);
    setMouseTracking(true);
    setMouseTrackingAllChildren();
    set_type("void");

    // Set handlers for GConf entries
    QObject::connect(&position_on_grid, SIGNAL(valueChanged()), this, SLOT(update_position()));
    QObject::connect(&size_on_grid, SIGNAL(valueChanged()), this, SLOT(update_size()));
    QObject::connect(&gconf_entry, SIGNAL(valueChanged()), this, SLOT(widget_removed()));
    QObject::connect(&visible, SIGNAL(valueChanged()), this, SLOT(changed_visibility()));
    QObject::connect(&style, SIGNAL(valueChanged()), this, SLOT(changed_style()));
    QObject::connect(&title, SIGNAL(valueChanged()), this, SLOT(changed_title()));
    QObject::connect(&favicon, SIGNAL(valueChanged()), this, SLOT(changed_favicon()));
    QObject::connect(&show_shadow, SIGNAL(valueChanged()), this, SLOT(toggle_shadow()));

    // Set handlers for the QX11EmbedContainer
    QObject::connect(embed_client, SIGNAL(clientClosed()), this, SLOT(client_closed()));
    QObject::connect(embed_client, SIGNAL(clientIsEmbedded()), this, SLOT(client_embedded()));
    QObject::connect(embed_client, SIGNAL(error(QX11EmbedContainer::Error)), this, SLOT(embedding_failed(QX11EmbedContainer::Error)));

    background->setLayout(main_layout);

    emit update_position();
    emit update_size();
    emit changed_style();
    emit changed_title();
    emit changed_favicon();
    emit changed_alpha();
    emit toggle_shadow();

    if (embed)
        init_embedded();

    wait_animation->show();
    show();
}

BloomWidgetContainer::~BloomWidgetContainer()
{
    if (remove_gconf) {
        if (deleted.value().isNull())
            gconf_entry.unset();
        else
            deleted.set(true);
    }
}

void BloomWidgetContainer::init_embedded()
{
    // Embed the remote window into the widget
    proxy = new BloomWidgetProxy(service, "/Widget", QDBusConnection::sessionBus(), desktop);

    // There are two options for a widget :
    //  - either embed itself into the provided container
    //  - either return a window and have it swallowed by the container

    if (!create_call_watcher && !embedded) {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(int(embed_client->winId())) << qVariantFromValue(gconf_entry.key());
        QDBusMessage dbus_msg = QDBusMessage::createMethodCall(proxy->service(), proxy->path(), proxy->interface(), QLatin1String("create"));
        dbus_msg.setArguments(argumentList);
        QDBusPendingReply<int> reply = QDBusConnection::sessionBus().asyncCall(dbus_msg, WIDGET_CREATION_TIMEOUT * 1000);
        create_call_watcher = new QDBusPendingCallWatcher(reply, this);
        connect(create_call_watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(widget_created(QDBusPendingCallWatcher*)));
    }
}

void BloomWidgetContainer::widget_created(QDBusPendingCallWatcher* pending_call)
{
    if (pending_call->isError()) {
        qDebug() << "Error" << gconf_entry.key() << pending_call->error().message();
        /*
        hide();
        close();
        return;
        */
    }
    QDBusPendingReply<int> reply = *pending_call;
    if (!winid)
        winid = reply;
    embed_widget();
    delete create_call_watcher;
    create_call_watcher = NULL;
}

void BloomWidgetContainer::embed_widget()
{
    if (!embedded && winid) {
        embed_client->embedClient(winid);
        embedded = true;
    }
}

void BloomWidgetContainer::add_css_file(QString css_path, QString css_file)
{
    QString theme = "";
    QFile file(css_path + "/" + css_file);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("File can't be found or opened");
    } else {
        QTextStream stream(&file);
        while(!stream.atEnd()) {
            theme += stream.readLine();
        }
    }
    file.close();
    theme.replace("THEMEDIR", css_path);
    setStyleSheet(theme);
}

const QRect BloomWidgetContainer::rect()
{
    return QRect(pos(), size());
}

void BloomWidgetContainer::set_frame_border_from_style_sheet()
{
    int border = -1;
    frame_border_bottom = -1;
    frame_border_left = -1;
    frame_border_right = -1;
    frame_border_top = -1;
    QStringList style_list = styleSheet().split('}');
    QStringList frame_style_list;
    bool found = false;
    for (int i = 0; i < style_list.size() && !found; i++) {
        if (style_list.at(i).contains("QWidget#frame")) {
            QString frame_style = style_list.at(i).section('{', 1, 1);
            frame_style_list = frame_style.split(';');
            found = true;
        }
    }

    QString tmp_value;
    bool ok;
    for (int i = 0; i < frame_style_list.size(); i++) {
        if (frame_style_list.at(i).contains("border") && frame_style_list.at(i).contains("width")) {
            tmp_value = frame_style_list.at(i).section(':', 1, 1);
            tmp_value.remove("px", Qt::CaseInsensitive);
            if (frame_style_list.at(i).contains("top")) {
                frame_border_top = tmp_value.toInt(&ok);
                if (!ok)
                    frame_border_top = -1;
            } else  if (frame_style_list.at(i).contains("right")) {
                frame_border_right = tmp_value.toInt(&ok);
                if (!ok)
                    frame_border_right = -1;
            } else  if (frame_style_list.at(i).contains("bottom")) {
                frame_border_bottom = tmp_value.toInt(&ok);
                if (!ok)
                    frame_border_bottom = -1;
            } else  if (frame_style_list.at(i).contains("left")) {
                frame_border_left = tmp_value.toInt(&ok);
                if (!ok)
                    frame_border_left = -1;
            } else {
                border = tmp_value.toInt(&ok);
                if (!ok)
                    border = -1;
            }
        }
    }

    if (frame_border_top == -1) {
        if (border > -1)
            frame_border_top = border;
        else
            frame_border_top = 0;
    }

    if (frame_border_right == -1) {
        if (border > -1)
            frame_border_right = border;
        else
            frame_border_right = 0;
    }

    if (frame_border_bottom == -1) {
        if (border > -1)
            frame_border_bottom = border;
        else
            frame_border_bottom = 0;
    }

    if (frame_border_left == -1) {
        if (border > -1)
            frame_border_left = border;
        else
            frame_border_left = 0;
    }

    frame_border_left = margin;
    frame_border_right = margin;
    frame_border_top = margin;
    frame_border_bottom = margin;
}

void BloomWidgetContainer::set_minimum_size_on_grid (int width, int height)
{
    minimum_size_on_grid = QSize(width, height);
}

void BloomWidgetContainer::set_maximum_size_on_grid (int width, int height)
{
    maximum_size_on_grid = QSize(width, height);
}

void BloomWidgetContainer::set_on_the_grid(bool is_on, bool vert, int thickness)
{
    return;
    on_the_grid = is_on;
    if (!on_the_grid) {
        vertical = vert;
        setMaximumSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
        setMinimumSize(QSize(0, 0));
        if (vertical)
            resize(QSize(thickness, desktop->height()));
        else
            resize(QSize(desktop->width(), thickness));

        header_left->hide();
        header->hide();
        header_right->hide();
        frame->setStyleSheet("QWidget#frame  {"
                             " background-color:none;"
                             " background-image:url("THEMEDIR"/alpha_0.0.png);"
                             " border-style:none;"
                             "}");
    }
}

void BloomWidgetContainer::set_title(QString txt)
{
    int w = 0;
    title = txt;
    title_label->setText(txt);
    title_label->adjustSize();
    int size_for_label = header->width() -
                             (button_delete && button_delete->isVisible() ? BUTTON_WIDTH : 0) -
                             (button_options && button_options->isVisible() ? BUTTON_WIDTH : 0) -
                             (!QString(favicon).isEmpty() ? 18 : 0);
    if (!QString(favicon).isEmpty()) {
        w = 18;
        favicon_label->resize(header->width(), favicon_label->height());
        favicon_label->move(0, (HEADER_HEIGHT - favicon_label->height()) / 2 + 2);
    }
    if (title_label->width() <= size_for_label) {
        title_label->resize(header->width(), title_label->height());
        title_label->move(w, (HEADER_HEIGHT - title_label->height()) / 2 + 2);
    } else {
        QString text = title_label->text();
        text.resize(text.size() - 3);
        while (title_label->width() + 1 > size_for_label && text.size() > 0) {
            text.resize(text.size() - 1);
            title_label->setText(text + "...");
            title_label->adjustSize();
        }
        title_label->resize(width(), title_label->height());
        title_label->move(w, (HEADER_HEIGHT - title_label->height()) / 2 + 2);
    }
    title_label->adjustSize();
}

void BloomWidgetContainer::update_position()
{
    if (on_the_grid) {
        QPoint position = grid->square_to_px(QPoint(position_on_grid));
        printf("Update position : (%d, %d) from (%d, %d)\n", position.x(), position.y(), QPoint(position_on_grid).x(), QPoint(position_on_grid).y());
        move(position);
    }
}

void BloomWidgetContainer::update_size()
{
    if (on_the_grid) {
        setMinimumSize(grid->square_to_px(QSize(minimum_size_on_grid)));
        setMaximumSize(grid->square_to_px(QSize(maximum_size_on_grid)));

        QSize size = grid->square_to_px(QSize(size_on_grid));
        resize(size);
    }
    emit changed_title();
}

void BloomWidgetContainer::update_shadow()
{
    if (shadow) {
        shadow->resize(size() + QSize(SHADOW_WIDTH * 2, SHADOW_WIDTH * 2));
        shadow->move(pos() - QPoint(SHADOW_WIDTH, SHADOW_WIDTH));
        shadow->lower();
    }
}

QStringList BloomWidgetContainer::get_available_styles()
{
    QStringList styles = QStringList();
    QDir themes_dir(THEMEDIR);
    QStringList themes = themes_dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for (int i = 0; i < themes.size(); i++) {
        styles.append(themes[i]);
    }
    return styles;
}

/*******************************
 *      Protected Methodes     *
 *******************************/

/* change the background of this bloom-widget with an image */
void BloomWidgetContainer::set_background(QImage img, bool auto_resize)
{
    background_img = img.copy(img.rect());
    if (!img.isNull()) {
         if (auto_resize)
                 background_auto_resize = 2;
         else
                 background_auto_resize = 0;

         /*
          * delete the "background" and "border" elements from the steel sheet
          * this elements are not compatible with an Image background
          */
         QStringList styleSheet = background->styleSheet().split(";");
         background->setStyleSheet("");

         for (int i = 0; i < styleSheet.size(); i++) {
             if (styleSheet.at(i).contains("background", Qt::CaseInsensitive) ||
                   styleSheet.at(i).contains("border", Qt::CaseInsensitive) ) {
                 styleSheet.removeAt(i--);
             }
         }

         background->setStyleSheet(styleSheet.join(";"));
         background->setAutoFillBackground(true);
         QBrush brush = QBrush();
         brush.setTextureImage(background_img.scaled(background->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation));
         palette.setBrush(QPalette::Window, brush);
         // background->setPalette(palette);
    } else {
        background->setStyleSheet("QWidget#background  {"
                                    "background-color:none;"
                                    "background-image:url("THEMEDIR"/alpha_0.0.png);"
                                    "border-style:none;"
                                "}");
    }
}

/* change the background of this bloom-widget with a pixmap */
void BloomWidgetContainer::change_background(QPixmap pix, bool auto_resize)
{
    background_pix = pix.copy(pix.rect());
    if (!pix.isNull()) {
         if (auto_resize)
             background_auto_resize = 1;
         else
             background_auto_resize = 0;

         /*
          * delete the "background" and "border" elements from the steel sheet
          * this elements are not compatible with a Pixmap background
          */
         QStringList styleSheet = background->styleSheet().split(";");
         background->setStyleSheet("");

         for (int i = 0; i < styleSheet.size(); i++) {
             if (styleSheet.at(i).contains("background", Qt::CaseInsensitive) ||
                   styleSheet.at(i).contains("border", Qt::CaseInsensitive) ) {
                 styleSheet.removeAt(i--);
             }
         }

         background->setStyleSheet(styleSheet.join(";"));
         background->setAutoFillBackground(true);
         QBrush brush = QBrush();
         brush.setTexture(background_pix.scaled(background->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation));
         palette.setBrush(QPalette::Window, brush);
         // background->setPalette(palette);
    } else {
        background->setStyleSheet("QWidget#background  {"
                                    "background-color:none;"
                                    "background-image:url("THEMEDIR"/alpha_0.0.png);"
                                    "border-style:none;"
                                "}");
    }
}

void BloomWidgetContainer::move_square_by_square(QPoint new_pos)
{
    QPoint p = grid->px_to_square(new_pos);
    move(grid->square_to_px(p));
}

void BloomWidgetContainer::set_type(std::string type)
{
    type = type;
}

void BloomWidgetContainer::setMouseTrackingAllChildren()
{
    setMouseTracking(true);
    for (int i = 0; i < children().size(); i++)
        if (children().at(i)->isWidgetType())
            ((BloomWidgetContainer*) children().at(i))->setMouseTrackingAllChildren();
}

void BloomWidgetContainer::close()
{
    /*
    if(proxy->before_destroy()) {
        return;
    }
    */
    QWidget::close();
    if (shadow) {
        delete shadow;
        shadow = NULL;
    }
    desktop->remove_widget(this);
}

void BloomWidgetContainer::remove()
{
    remove_gconf = true;
    close();
}

void BloomWidgetContainer::show_caption(bool state)
{
    if (state) {
        header->show();
        header_left->show();
        header_right->show();
    }
    else {
        header->hide();
        header_left->hide();
        header_right->hide();
        header_left->raise();
        header_right->raise();
        header->raise();
    }
}

void BloomWidgetContainer::resizeEvent(QResizeEvent* event)
{
    if(manual_resize == true && on_the_grid) {
        QSize(size_on_grid).scale(grid->px_to_square(event->size()),
                                  Qt::IgnoreAspectRatio);
    }

    if (on_the_grid) {
        header->resize(width() - (HEADER_CORNERS_WIDTH + MARGE)* 2, HEADER_HEIGHT);
        header_left->move(MARGE, MARGE);
        header->move(HEADER_CORNERS_WIDTH + MARGE, MARGE);
        header_right->move(width() - HEADER_CORNERS_WIDTH - MARGE, MARGE);

        frame->resize(width() - MARGE * 2, height() - HEADER_HEIGHT - MARGE * 2);
        frame->move(MARGE, HEADER_HEIGHT + MARGE);
    } else {
        // resize(event->size());
        frame->resize(size());
    }
    background->resize(frame->size()-QSize(frame_border_left + frame_border_right, frame_border_top + frame_border_bottom));
    background->move(QPoint(frame_border_left, frame_border_top));

    if (background_auto_resize != 0) {
        QBrush brush = QBrush();
        if (background_auto_resize == 1) {
            QPixmap pix = background_pix.scaled(background->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            brush.setTexture(pix);
        }
        else {
            QImage img = background_img.scaled(background->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            brush.setTextureImage(img);
        }
        palette.setBrush(QPalette::Window, brush);
        // background->setPalette(palette);
    }

    if (button_delete) {
        if (on_the_grid)
            button_delete->move(header->width() - BUTTON_WIDTH, (HEADER_HEIGHT - BUTTON_HEIGHT) / 2 + 2);
        else
            button_delete->move(width() - BUTTON_WIDTH - 2, 2);
    }
    if (button_options) {
        if (on_the_grid)
            button_options->move(header->width() - BUTTON_WIDTH * (button_delete ? 2 : 1) - 2, (HEADER_HEIGHT - BUTTON_HEIGHT) / 2 + 2);
        else
            button_options->move(width() - BUTTON_WIDTH * (button_delete ? 2 : 1) - 4, 2);
    }

    if (on_the_grid)
        move(grid->square_to_px(QPoint(position_on_grid)));

    event->accept();
    background->lower();
    header_left->raise();
    header_right->raise();
    header->raise();

    if (shadow)
        update_shadow();

    emit changed_title();
}

/* When the mouse leave the bloom-widget area */
void BloomWidgetContainer::leaveEvent (QEvent *event)
{
    setMouseTracking(false);

    if (QApplication::mouseButtons() == Qt::NoButton)
        manual_resize = false;

    if (auto_hide_caption) {
        show_caption(false);
    }

    QApplication::setOverrideCursor(Qt::ArrowCursor);
    if (button_delete)
        button_delete->hide();
    if (button_options)
        button_options->hide();

    event->accept();
    desktop->selected_widget = NULL;
}

/* When the mouse is hovering the bloom-widget area */
void BloomWidgetContainer::mouseMoveEvent (QMouseEvent *event)
{
    desktop->selected_widget = this;
    desktop->setFocus(Qt::MouseFocusReason);

    if (!fixed && on_the_grid) {           /* bloom-widget is not fixed */
        QPoint local_cursor_position = mapFromGlobal(QCursor::pos());

        if (auto_hide_caption) {
            show_caption(true);
        }

        /*
         * cursor localisation in the bloom_widget
         * and the manual resize if allowed
         */
        if (!manual_resize) {
            /*
             * if position_cursor equal 0,
             * the mouse cursor is not on the border of the widget
             * else if position_cursor equal 1 to 8,
             * the mouse cursor is on the border of the widget
             * which allows the manuel resize
             */
            position_cursor = 0;
            if ((button_delete && button_delete->underMouse()) ||
                (button_options && button_options->underMouse())) {
                QApplication::setOverrideCursor(Qt::ArrowCursor);
            } else if (allow_manual_resize) {
                if (local_cursor_position.x() < 10) {
                    if (local_cursor_position.y() < 10) {
                        QApplication::setOverrideCursor(Qt::SizeFDiagCursor);
                        position_cursor = 1;
                    } else if (local_cursor_position.y() > (size().height() - 10)) {
                        QApplication::setOverrideCursor(Qt::SizeBDiagCursor);
                        position_cursor = 2;
                    } else {
                        QApplication::setOverrideCursor( Qt::SizeHorCursor);
                        position_cursor = 3;
                    }
                } else if (local_cursor_position.x() > (size().width() - 10)) {
                    if (local_cursor_position.y() < 10) {
                        QApplication::setOverrideCursor(Qt::SizeBDiagCursor);
                        position_cursor = 4;
                    } else if (local_cursor_position.y() > (size().height() - 10)) {
                        QApplication::setOverrideCursor(Qt::SizeFDiagCursor);
                        position_cursor = 5;
                    } else {
                        QApplication::setOverrideCursor( Qt::SizeHorCursor);
                        position_cursor = 6;
                    }
                } else if (local_cursor_position.y() < 10) {
                    QApplication::setOverrideCursor(Qt::SizeVerCursor);
                    position_cursor = 7;
                } else if (local_cursor_position.y() > (size().height() - 10)) {
                    QApplication::setOverrideCursor(Qt::SizeVerCursor);
                    position_cursor = 8;
                } else if (header_left->geometry().contains(local_cursor_position) ||
                           header->geometry().contains(local_cursor_position) ||
                           header_right->geometry().contains(local_cursor_position)) {
                    QApplication::setOverrideCursor(Qt::SizeAllCursor);
                } else {
                    QApplication::setOverrideCursor(Qt::ArrowCursor);
                }
            } else {
                if (header_left->geometry().contains(local_cursor_position) ||
                            header->geometry().contains(local_cursor_position) ||
                            header_right->geometry().contains(local_cursor_position)) {
                    QApplication::setOverrideCursor(Qt::SizeAllCursor);
                } else {
                    QApplication::setOverrideCursor(Qt::ArrowCursor);
                }
            }
        } else if (desktop->mapFromGlobal(QCursor::pos()).x() >= 0 && desktop->mapFromGlobal(QCursor::pos()).y() >= 0 ) {
            /*
             * else,
             * launch the right resize in function of the position_cursor
             */

            QPoint p = grid->px_to_square(desktop->mapFromGlobal(QCursor::pos()));
            p = grid->square_to_px(p);

            switch(position_cursor) {
                case 1: resize_top_left(p); break;
                case 2: resize_bottom_left(p, local_cursor_position); break;
                case 3: resize_left(p); break;
                case 4: resize_top_right(p, local_cursor_position); break;
                case 5: resize_bottom_right(p); break;
                case 6: resize_right(p); break;
                case 7: resize_top(p); break;
                case 8: resize_bottom(p); break;
            }
        }
        event->accept();
    } else {
        event->ignore();
    }
}

void BloomWidgetContainer::mousePressEvent(QMouseEvent *event)
{
    menu->hide();

    if (position_cursor != 0 && QApplication::mouseButtons() == Qt::LeftButton) {
        manual_resize = true;
        event->accept();
    } else if (QApplication::mouseButtons() == Qt::LeftButton &&
               (header_left->geometry().contains(mapFromGlobal(QCursor::pos())) ||
                header->geometry().contains(mapFromGlobal(QCursor::pos())) ||
                header_right->geometry().contains(mapFromGlobal(QCursor::pos())))) {
        event->ignore();
    }
}

/* On mouse release event */
void BloomWidgetContainer::mouseReleaseEvent(QMouseEvent *event)
{
    size_on_grid.set(QSize(grid->px_to_square(size())), false);
    resize(grid->square_to_px(QSize(size_on_grid)));
    move(grid->square_to_px(QPoint(position_on_grid)));
    if (position_cursor != 0) {
        manual_resize = false;
        event->accept();
    } else {
        event->ignore();
    }
}

void BloomWidgetContainer::contextMenuEvent(QContextMenuEvent *event)
{
    if (has_options) {
        menu->show_menu(event->globalPos());
    }
}

/* When the mouse enter in the bloom-widget area */
void BloomWidgetContainer::enterEvent(QEvent *event)
{
    if (QApplication::mouseButtons() == Qt::NoButton)
        manual_resize = false;

    if (button_delete) {
        button_delete->show();
        button_delete->raise();
    }

    if (button_options) {
        button_options->show();
        button_options->raise();
    }

    header->raise();
    event->accept();

    if (!is_on_the_grid()) {
        setMouseTracking(false);
        QApplication::setOverrideCursor(Qt::ArrowCursor);
    }

    embed_client->activateWindow();
    embed_client->raise();
    embed_client->setFocus(Qt::MouseFocusReason);

    setMouseTracking(true);
    setMouseTrackingAllChildren();
}

void BloomWidgetContainer::moveEvent(QMoveEvent* event)
{
    position_on_grid.set(grid->px_to_square(event->pos()), false);
    QWidget::moveEvent(event);
    if (shadow)
        update_shadow();
    event->accept();
}

void BloomWidgetContainer::widget_removed()
{
    close();
}

void BloomWidgetContainer::client_closed()
{
    qWarning() << "Widget" << gconf_entry.key() << "closed. Re-embedding" << winid;
    embedded = false;
    embed_widget();
}

void BloomWidgetContainer::client_embedded()
{
    qDebug() << "Widget" << gconf_entry.key() << "is embedded" << embed_client->clientWinId();
    bool auto_embed = winid == 0;
    embedded = true;
    winid = embed_client->clientWinId();
    if (wait_animation) {
        wait_animation->hide();
        main_layout->removeWidget(wait_animation);
        main_layout->addWidget(embed_client);
        update();
        if (auto_embed) {
            startTimer(3000);
        }
        delete wait_animation;
        delete wait_movie;
        wait_animation = NULL;
        wait_movie = NULL;
    }
}

void BloomWidgetContainer::timerEvent(QTimerEvent *event)
{
    update();
    // embed_client->resize(embed_client->size() + QSize(1, 1));
    killTimer(event->timerId());
}


void BloomWidgetContainer::embedding_failed(QX11EmbedContainer::Error error)
{
    qDebug() << "Embedding of" << gconf_entry.key() << "failed";
    hide();
    close();
}

void BloomWidgetContainer::changed_style()
{
    QString theme = THEMEDIR;
    theme += "/" + QString(style);
    add_css_file(theme, "bloom-widget.css");
    set_frame_border_from_style_sheet();
}

void BloomWidgetContainer::changed_visibility()
{
    if (bool(visible)) {
        show();
    }
    else {
        hide();
    }
}

void BloomWidgetContainer::changed_title()
{
    if(!QString(title).isEmpty())
        set_title(QString(title));
}

void BloomWidgetContainer::changed_favicon()
{
    if (!QString(favicon).isEmpty()) {
        GtkIconTheme *icon_theme = gtk_icon_theme_get_default();
        GtkIconInfo *icon_info = gtk_icon_theme_lookup_icon(icon_theme,
                                                            QString(favicon).toLatin1(),
                                                            16,
                                                            GTK_ICON_LOOKUP_USE_BUILTIN);
        const gchar *icon_filename = gtk_icon_info_get_filename(icon_info);
        if (icon_filename) {
            favicon_icon = new QIcon(QString(icon_filename));
            favicon_label->setPixmap(favicon_icon->pixmap(16, 16));
            favicon_label->show();
        }
    }
    else {
        favicon_label->hide();
    }
    emit changed_title();
}

void BloomWidgetContainer::toggle_shadow()
{
    if(show_shadow && !shadow) {
        shadow = new QWidget(desktop);
        update_shadow();
        shadow->setObjectName("shadow");
        shadow->show();
        shadow->lower();
    }
    else {
        delete shadow;
        shadow = NULL;
    }
}

void BloomWidgetContainer::changed_alpha()
{
    if((int) alpha != 255) {
        background->setAttribute(Qt::WA_TranslucentBackground, true);
        frame->setAttribute(Qt::WA_TranslucentBackground, true);
        setAttribute(Qt::WA_TranslucentBackground, true);
    } else {
        background->setAttribute(Qt::WA_TranslucentBackground, false);
        frame->setAttribute(Qt::WA_TranslucentBackground, false);
        setAttribute(Qt::WA_TranslucentBackground, false);
    }
}

/*  ----------- Resize conditions ------------ */

void BloomWidgetContainer::resize_bottom(QPoint local_cursor_position)
{
    /* bottom */
    int new_height = local_cursor_position.y() - y();

    /* limit conditions */
    if ((y() + new_height) > desktop->height())
        return;

    QRect rect_tmp = QRect(x(), y(), width(), new_height);

    /* Collision with other bloom_widget */
    if (grid->collision(rect_tmp, this)) {
        grid->nearest_collision(rect());
        vector<int> vTmp = grid->get_crossed();
        QRect crossed_rect;
        QRect modif;
        int size = vTmp.size();
        for (int i = 0; i < size; i++) {
            if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                modif = rect_tmp.intersected(crossed_rect);

                new_height = modif.y() - y();
            }
        }
    }
    resize(width(), new_height);
}

void BloomWidgetContainer::resize_bottom_left(QPoint p, QPoint local_cursor_position)
{
    /* bottom-left */
    int new_width = width() + x() - p.x();
    int new_height = local_cursor_position.y();

    /* limit conditions */
    if (p.x() < 0) {
        new_width = rect().bottomRight().x();
        p.setX(0);
    } else if (new_width > maximumWidth()) {
        new_width = maximumWidth();
        p.setX(rect().bottomRight().x() - maximumWidth() + 1);
    }
    if (new_width < minimumWidth()) {
        new_width = minimumWidth();
        p.setX(rect().bottomRight().x() - minimumWidth() + 1);
    }
    if (new_height < minimumHeight())
        new_height = minimumHeight();

    if ((y() + new_height) > desktop->height())
        new_height = desktop->height() - y();

    int k = 0;
    while (k++ != 2) {
        QRect rect_tmp = QRect(p.x(), y(), new_width, new_height);

        /* Collision with other bloom_widget */
        if (grid->collision(rect_tmp, this)) {
            grid->nearest_collision(rect());
            vector<int> vTmp = grid->get_crossed();
            QRect crossed_rect;
            QRect modif;
            int size = vTmp.size();
            for (int i = 0; i < size; i++) {
                if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                    crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                    modif = rect_tmp.intersected(crossed_rect);

                    if ((rect_tmp.right() - minimumWidth()) < crossed_rect.right() ||
                          (rect_tmp.top() + minimumHeight()) > crossed_rect.top()) {
                        if ((rect_tmp.right() - minimumWidth()) < crossed_rect.right())
                            new_height = modif.y() - y();
                        if ((rect_tmp.top() + minimumHeight()) > crossed_rect.top()) {
                            new_width = rect().right() - modif.right();
                            p.setX(crossed_rect.right()+1);
                        }
                    } else {
                        if (modif.height() > modif.width()) {
                            new_width = rect().right() - modif.right();
                            p.setX(crossed_rect.right()+1);
                        } else {
                            new_height = modif.y() - y();
                        }
                    }
                }
            }
        }
    }
    setGeometry(p.x(), y(), new_width, new_height);
}

void BloomWidgetContainer::resize_bottom_right(QPoint local_cursor_position)
{
    /* bottom-right */
    int new_width = local_cursor_position.x() - x();
    int new_height = local_cursor_position.y() - y();

    /* limit conditions */
    if ((x() + new_width) > desktop->width())
         new_width = desktop->width() - x();
    if ((y() + new_height) > desktop->height())
         new_height = desktop->height() - y();
    if (new_width < minimumWidth())
         new_width = minimumWidth();
    if (new_height < minimumHeight())
         new_height = minimumHeight();

    int k = 0;
    while(k++!=2) {
        QRect rect_tmp = QRect(x(), y(), new_width, new_height);

        /* Collision with other bloom_widget */
        if (grid->collision(rect_tmp, this)) {
            grid->nearest_collision(rect());
            vector<int> vTmp = grid->get_crossed();
            QRect crossed_rect;
            QRect modif;
            int size = vTmp.size();
            for (int i = 0; i < size; i++) {
                if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                    crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();
                    modif = rect_tmp.intersected(crossed_rect);

                    if ((rect_tmp.left() + minimumWidth()) > crossed_rect.left() ||
                          (rect_tmp.top() + minimumHeight()) > crossed_rect.top()) {
                        if ((rect_tmp.left() + minimumWidth()) > crossed_rect.left())
                            new_height = modif.y() - y();
                        if ((rect_tmp.top() + minimumHeight()) > crossed_rect.top())
                            new_width = modif.x() - x();
                    } else {
                        if (modif.height() > modif.width())
                            new_width = modif.x() - x();
                        else
                            new_height = modif.y() - y();
                    }
                }
            }
        }
    }
    resize(new_width, new_height);
}

void BloomWidgetContainer::resize_left(QPoint p)
{
    /* left */
    int new_width = width() + x() - p.x();

    /* limit conditions */
    if (p.x() < 0) {
        new_width = rect().bottomRight().x();
        p.setX(0);
    } else if (new_width > maximumWidth()) {
        new_width = maximumWidth();
        p.setX(rect().bottomRight().x() - maximumWidth() + 1);
    }
    if (new_width < minimumWidth()) {
        new_width = minimumWidth();
        p.setX(rect().bottomRight().x() - minimumWidth() + 1);
    }

    QRect rect_tmp = QRect(p.x(), y(), new_width, height());

    /* Collision with other bloom_widget */
    if (grid->collision(rect_tmp, this)) {
        grid->nearest_collision(rect());
        vector<int> vTmp = grid->get_crossed();
        QRect crossed_rect;
        QRect modif;
        int size = vTmp.size();
        for (int i = 0; i < size; i++) {
            if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                modif = rect_tmp.intersected(crossed_rect);

                new_width = rect().right() - modif.right();
                p.setX(crossed_rect.right()+1);
            }
        }
    }

    setGeometry(p.x(), y(), new_width, height());
}

void BloomWidgetContainer::resize_right(QPoint local_cursor_position)
{
    /* right */
    int new_width = local_cursor_position.x() - x();

    /* limit conditions */
    if ((x() + new_width) > desktop->width())
         return;

    QRect rect_tmp = QRect(x(), y(), new_width, height());

    /* Collision with other bloom_widget */
    if (grid->collision(rect_tmp, this)) {
        grid->nearest_collision(rect());
        vector<int> vTmp = grid->get_crossed();
        QRect crossed_rect;
        QRect modif;
        int size = vTmp.size();
        for (int i = 0; i < size; i++) {
            if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                modif = rect_tmp.intersected(crossed_rect);
                new_width = modif.x() - x();
            }
        }
    }

    setGeometry(x(), y(), new_width, height());
}

void BloomWidgetContainer::resize_top(QPoint p)
{
    /* top */
    int new_height = height() + y() - p.y();

    /* limit conditions */
    if (p.y() < 0) {
        new_height = rect().bottomRight().y();
        p.setY(0);
    } else if (new_height > maximumHeight()) {
        new_height = maximumHeight();
        p.setY(rect().bottomRight().y() - maximumHeight() + 1);
    }
    if (new_height < minimumHeight()) {
        new_height = minimumHeight();
        p.setY(rect().bottomRight().y() - minimumHeight() + 1);
    }

    QRect rect_tmp = QRect(x(), p.y(), width(), new_height);

    /* Collision with other bloom_widget */
    if (grid->collision(rect_tmp, this)) {
        grid->nearest_collision(rect());
        vector<int> vTmp = grid->get_crossed();
        QRect crossed_rect;
        QRect modif;
        int size = vTmp.size();
        for (int i = 0; i < size; i++) {
            if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                modif = rect_tmp.intersected(crossed_rect);

                new_height = rect().bottom() - modif.bottom();
                p.setY(crossed_rect.bottom()+1);
            }
        }
    }

    setGeometry(x(), p.y(), width(), new_height);
}

void BloomWidgetContainer::resize_top_left(QPoint p)
{
    /* top-left */
    int new_width = width() + x() - p.x();
    int new_height = height() + y() - p.y();

    /* limit conditions */
    if (p.x() < 0) {
        new_width = rect().bottomRight().x();
        p.setX(0);
    } else if (new_width > maximumWidth()) {
        new_width = maximumWidth();
        p.setX(rect().bottomRight().x() - maximumWidth() + 1);
    }
    if (p.y() < 0) {
        new_height = rect().bottomRight().y();
        p.setY(0);
    } else if (new_height > maximumHeight()) {
        new_height = maximumHeight();
        p.setY(rect().bottomRight().y() - maximumHeight() + 1);
    }
    if (new_width < minimumWidth()) {
        new_width = minimumWidth();
        p.setX(rect().bottomRight().x() - minimumWidth() + 1);
    }
    if (new_height < minimumHeight()) {
        new_height = minimumHeight();
        p.setY(rect().bottomRight().y() - minimumHeight() + 1);
    }

    int k = 0;
    while(k++!=2) {
        QRect rect_tmp = QRect(p.x(), p.y(), new_width, new_height);

        /* Collision with other bloom_widget */
        if (grid->collision(rect_tmp, this)) {
            grid->nearest_collision(rect());
            vector<int> vTmp = grid->get_crossed();
            QRect crossed_rect;
            QRect modif;
            int size = vTmp.size();
            for (int i = 0; i < size; i++) {
                if (desktop->widgets.at(vTmp.at(i))->is_on_the_grid()) {
                    crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                    modif = rect_tmp.intersected(crossed_rect);

                    if ((rect_tmp.right() - minimumWidth()) < crossed_rect.right() ||
                          (rect_tmp.bottom() - minimumHeight()) < crossed_rect.bottom()) {
                        if ((rect_tmp.right() - minimumWidth()) < crossed_rect.right()) {
                            new_height = rect().bottom() - modif.bottom();
                            p.setY(crossed_rect.bottom()+1);
                        }
                        if ((rect_tmp.bottom() - minimumHeight()) < crossed_rect.bottom()) {
                            new_width = rect().right() - modif.right();
                            p.setX(crossed_rect.right()+1);
                        }
                    } else {
                        if ((modif.height() > modif.width())) {
                            new_width = rect().right() - modif.right();
                            p.setX(crossed_rect.right()+1);
                        } else {
                            new_height = rect().bottom() - modif.bottom();
                            p.setY(crossed_rect.bottom()+1);
                        }
                    }
                }
            }
        }
    }
    setGeometry(p.x(), p.y(), new_width, new_height);
}

void BloomWidgetContainer::resize_top_right(QPoint p, QPoint local_cursor_position)
{
    /* top-rigth */
    int new_width = local_cursor_position.x();
    int new_height = height() + y() - p.y();

    /* limit conditions */
    if (p.y() < 0) {
        new_height = rect().bottomRight().y();
        p.setY(0);
    } else if (new_height > maximumHeight()) {
        new_height = maximumHeight();
        p.setY(rect().bottomRight().y() - maximumHeight() + 1);
    }

    if ((x() + new_width) > desktop->width())
         new_width = desktop->width() - x();

    if (new_height < minimumHeight()) {
        new_height = minimumHeight();
        p.setY(rect().bottomRight().y() - minimumHeight() + 1);
    }

    int k = 0;
    while (k++ != 2) {
        QRect rect_tmp = QRect(x(), p.y(), new_width, new_height);

        /* Collision with other bloom_widget */
        if (grid->collision(rect_tmp, this)) {
            grid->nearest_collision(rect());
            vector<int> vTmp = grid->get_crossed();
            QRect crossed_rect;
            QRect modif;
            int size = vTmp.size();
            for (int i = 0; i < size; i++) {
                crossed_rect = desktop->widgets.at(vTmp.at(i))->rect();

                modif = rect_tmp.intersected(crossed_rect);

                if ((rect_tmp.right() - minimumWidth()) < crossed_rect.right() ||
                        (rect_tmp.top() + minimumHeight()) > crossed_rect.top()) {
                    if ((rect_tmp.right() - minimumWidth()) < crossed_rect.right()) {
                        new_height = rect().bottom() - modif.bottom();
                        p.setY(crossed_rect.bottom()+1);
                    }
                    if ((rect_tmp.top() + minimumHeight()) > crossed_rect.top())
                        new_width = modif.x() - x();
                } else {
                    if (modif.height() > modif.width()) {
                        new_width = modif.x() - x();
                    } else {
                        new_height = rect().bottom() - modif.bottom();
                        p.setY(crossed_rect.bottom()+1);
                    }
                }
            }
        }
    }

    setGeometry(x(), p.y(), new_width, new_height);
}

