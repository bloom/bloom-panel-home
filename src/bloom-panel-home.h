#ifndef BloomPanelHome_H_
#define BloomPanelHome_H_

#include <QtCore>
#include <QtGui>
#include <dlfcn.h>

#include "bloom-panel-desktop.h"
#include "gconfitem.h"

#define __GTK_BINDINGS_H__

#ifdef NO_BLOOM_PANEL
    class MplPanelClient;
#else
extern "C" {
    #include <moblin-panel/mpl-panel-common.h>
    #include <moblin-panel/mpl-panel-qt.h>
}
#endif

#define WIDGETS_DIR "/usr/lib/bloom-widgets/"
#define WIDGETS_GCONF_KEY "/desktop/bloom/panels/home/widgets"

class BloomWidgetPendingCallWatcher;
class BloomWidgetFactoryProxy;

struct PendingRequest
{
    QString service;
    QDBusObjectPath obj_path;
    QString gconf_entry;
    BloomWidgetContainer *widget;
    bool embed;
};

class BloomPanelHome : public QWidget
{
    Q_OBJECT

    public:
        BloomPanelHome(QWidget *parent = 0);
        virtual ~BloomPanelHome();

        void set_panel_client(MplPanelClient *p) { panel = p; }
        QString get_free_gconf_entry();
        BloomWidgetContainer* request_widget(QString interface, QString key, bool embed = true);
        BloomWidgetContainer *create_container_for_widget(QString service, QString type, QString path, QString key, bool embed = true);

    protected:
        void spawn_widget(QString interface);
        void changeEvent(QEvent* event);
        void mousePressEvent(QMouseEvent* event);
        void showEvent(QShowEvent *event);

    public slots:
        void load_widgets();
        void update_interface() {}
        void widget_list_changed(const QString& key, const QVariant& value);

    public Q_SLOTS:
        void drop_new_widget(const QString &interface, const QString &schema);
        void service_started(const QString &);

    private:
        QWidget                 *border;                /* border around the desktop */
        QWidget                 *border_lateral;        /* border around the sidebar */
        BloomPanelDesktop       *desktop;               /* the desktop */
        QVBoxLayout             *vlayout;

        QDBusServiceWatcher dbus_watcher;
        QList<PendingRequest> pending_requests;

        MplPanelClient          *panel;
        GConfItem               gconf_entry;
        bool loaded;
};

#endif /* BloomPanelHome_H_ */
