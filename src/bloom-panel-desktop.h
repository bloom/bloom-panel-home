#ifndef BLOOM_PANEL_H_
#define BLOOM_PANEL_H_
#define __GTK_BINDINGS_H__

#include <QtCore>
#include <QtGui>

#include "bloom-panel-desktop-grid.h"
#include "bloom-panel-desktop-context-menu.h"
#include "bloom-widget-container.h"
#include "gconfitem.h"

class QDragEnterEvent;
class QDropEvent;
class QMouseEvent;
class BloomPanelHome;

class BloomPanelDesktop : public QWidget
{
    Q_OBJECT

    public:
        BloomPanelDesktop(QWidget *parent = 0);

        void add_to_menu(QAction *action) { menu->addAction(action); }
        void add_to_menu(QMenu *new_menu) { menu->addMenu(new_menu); }
        BloomWidgetContainer* add_widget(BloomWidgetContainer *new_widget);
        void remove_widget(BloomWidgetContainer *new_widget);
        void load();
        void relayout();

        QImage get_background() { return background; }
        QRect get_highlighted_rect() { return highlighted_rect; }
        BloomPanelDesktopGrid* get_grid() { return grid; }
        QList<BloomWidgetContainer*> get_widgets() { return widgets; }

        void moveEvent(QMoveEvent*);
        void paintEvent(QPaintEvent *event);
        void resizeEvent(QResizeEvent*);
        void mouseMoveEvent(QMouseEvent*);
        void mouseReleaseEvent(QMouseEvent *event);
        void mousePressEvent(QMouseEvent *event);

    private slots:
        void choose_background          ();
        void launch_update_home         ()                      { emit update_home(); }
        void load_background            ();

    signals:
        void clicked            (BloomWidgetContainer*);
        void update_home        ();

    public:
        QWidget                             *background_opacity;
        QSize background_size;
        QPoint background_pos;

        int margin_left;
        int margin_right;
        int margin_top;
        int margin_bottom;

        BloomPanelDesktopGrid *grid;                  /* Desktop grid */
        BloomPanelDesktopContextMenu *menu;           /* Right-click menu */
        BloomPanelHome *parent;

        QImage background;             /* Resized background image */
        bool drop_in_progress;       /* If drop action is in progress */
        QRect highlighted_rect;        /* Rectangle to indicate a drop position */
        QWidget                             *highlighted_widget;
        bool                                is_out;                 /* If cursor is out the desktop area */
        QList<BloomWidgetContainer*>        widgets;                /* List of all bloom-widgets on the desktop */
        QPixmap                             pix;                    /* Pixmap paint on the highlightedRect */
        QPoint                              position_on_widget;     /* Cursor position on a selected widget */

        BloomWidgetContainer                *selected_widget;

        GConfItem                           background_url;
        GConfItem                           background_options;
};

#endif /* BLOOM_PANEL_H_ */
