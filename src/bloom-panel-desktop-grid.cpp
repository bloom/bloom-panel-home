/*
 * bloom-panel-desktop-grid
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: ROGER Josselin
 *         BAUBEAU Sylvain
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bloom-panel-desktop-grid.h"
#include "bloom-panel-desktop.h"

BloomPanelDesktopGrid::BloomPanelDesktopGrid(BloomPanelDesktop *parent)
{
    desktop = parent;
    crossed = new vector<int>;
    scope_of_positions = new vector<QPoint>;
    selected_origin = QPoint(0, 0);

    /* Update the parent size to a round number of squares */
    int dif_x = (parent->width() % GRID_SIZE_X);
    int dif_y = (parent->height() % GRID_SIZE_Y);
    if (dif_x != 0 &&  dif_y != 0)
        parent->resize(parent->width() - dif_x + GRID_SIZE_X, parent->height() - dif_y + GRID_SIZE_Y);
    else if (dif_x != 0 &&  dif_y == 0)
        parent->resize(parent->width() - dif_x + GRID_SIZE_X, parent->height());
    else if (dif_x == 0 &&  dif_y != 0)
        parent->resize(parent->width(), parent->height() - dif_y + GRID_SIZE_Y);

    /* resize the grid (square's size) */
    resize_grid(parent->size());
}

/* Verify if the selected widget (dragged widget) intersects others widgets */
bool BloomPanelDesktopGrid::collision(BloomWidgetContainer *widget)
{
    int i = 0;
    bool is_intersected = false;
    QRect selected = widget->rect();
    while (i < desktop->widgets.size() && !is_intersected) {
        BloomWidgetContainer *test_widget = desktop->widgets.at(i);
        if (widget != test_widget && test_widget->is_on_the_grid())
           is_intersected = selected.intersects(test_widget->rect());
        i++;
    }
    return is_intersected;
}

/* Verify if the indicated rectangle intersects a widget */
bool BloomPanelDesktopGrid::collision(QRect selected, BloomWidgetContainer *widget)
{
    bool is_intersected = false;
    int i = 0;
    if(!crossed->empty()) crossed->clear();
    while (i < desktop->widgets.size() && !is_intersected) {
        BloomWidgetContainer *test_widget = desktop->widgets.at(i);
        if (test_widget != widget && test_widget->is_on_the_grid()) {
            QRect rect = test_widget->rect();
            if (selected.intersects(test_widget->rect()))
                crossed->push_back(i);
        }
        is_intersected = !crossed->empty();
        i++;
    }

    return is_intersected;
}

/* Find the nearest free place to drop the dragged widget */
void BloomPanelDesktopGrid::find_nearest_place(QRect *highlightedRect, QPoint old_pos)
{
    /* Get the dragged widget position on the grid */
    bool is_in_the_scope = false;
    old_pos = put_on_grid(old_pos);
    QPoint new_pos;

    /* verify if the actual dragged widget's position is on a known free place */
    unsigned int i = 0;
    while (i < scope_of_positions->size() && !is_in_the_scope) {
        is_in_the_scope = old_pos == scope_of_positions->at(i);
        i++;
    }

    if(is_in_the_scope) {
        /* if it is */
        new_pos = old_pos;
    } else {
        /* else
         * for each known free place
         * get the distance between the widget's position and the free place's position
         * and find the smaller one
         */
        int old_range = 0;
        int range = 0;
        for(unsigned int j = 0; j < scope_of_positions->size(); j++) {
            QPoint point = scope_of_positions->at(j) - old_pos;
            if(j == 0) {
                range = point.manhattanLength();
                old_range = range;
                new_pos = scope_of_positions->at(j);
            } else {
                range = point.manhattanLength();
                if(range < old_range) {
                        old_range = range;
                        new_pos = scope_of_positions->at(j);
                }
            }
        }
    }

    /* move the highlightedRect to the new position */
    highlightedRect->moveTo(new_pos);
}

/*
 * Find all free places where the dragged widget can be drop
 * save it on the scope_of_positions variable
 */
int BloomPanelDesktopGrid::find_scope_of_positions(BloomWidgetContainer *widget)
{
    if(!scope_of_positions->empty()) scope_of_positions->clear();

    /* create a dragged widget's size like rectangle */
    QRect tmp = QRect(QPoint(), widget->rect().size());

    /* for each position on the grid verify if the tmp rectangle intersects a widget */
    for(int x = 0; x <= GRID_SIZE_X; x++) {
        for(int y = 0; y <= GRID_SIZE_Y; y++) {
            tmp.moveTo(x * grid_square_width, y * grid_square_height);
            if(!collision(tmp, widget) && tmp.right() <= desktop->width() && tmp.bottom() <= desktop->height()) {
                scope_of_positions->push_back(QPoint(x * grid_square_width, y * grid_square_height));
            }
        }
    }
    return scope_of_positions->size();
}

/* Find collision on rectangle resize */
void BloomPanelDesktopGrid::nearest_collision(QRect selected)
{
    int left = -1;
    int top = -1;
    int right = -1;
    int bottom = -1;
    int dist_left = -1;
    int dist_top = -1;
    int dist_right = -1;
    int dist_bottom = -1;
    int dist_tmp = -1;
    QRect tmp;
    int crossed_size = crossed->size();

    for (int i= 0; i < crossed_size; i++) {
        tmp = desktop->widgets.at(crossed->at(i))->rect();

        /* on left or right */
        if ((tmp.bottom() >= selected.top() && tmp.bottom() <= selected.bottom()) ||
            (tmp.top() >= selected.top() && tmp.top() <= selected.bottom()) ||
            (tmp.bottom() >= selected.bottom() && tmp.top() <= selected.top())) {
            /* on left */
            dist_tmp = selected.right() - tmp.right();
            if (dist_tmp > 0) {
                if (left == -1) {
                    left = i;
                    dist_left = dist_tmp;
                } else if (dist_tmp < dist_left) {
                    left = i;
                    dist_left = dist_tmp;
                }
            }

            /* on right */
            dist_tmp = tmp.left() - selected.left();
            if (dist_tmp > 0) {
                if (right == -1) {
                    right = i;
                    dist_right = dist_tmp;
                } else if (dist_tmp < dist_right) {
                    right = i;
                    dist_right = dist_tmp;
                }
            }
        }

        /* on top or bottom */
        if ((tmp.left() <= selected.right() && tmp.left() >= selected.left()) ||
                (tmp.right() <= selected.right() && tmp.right() >= selected.left()) ||
                (tmp.right() >= selected.right() && tmp.left() <= selected.left())) {
            /* on top */
            if ((selected.top() - tmp.bottom()) > 0) {
                if (top == -1) {
                    top = i;
                    dist_top = selected.top() - tmp.bottom();
                } else if ((selected.top() - tmp.bottom()) < dist_top) {
                    top = i;
                    dist_top = selected.top() - tmp.bottom();
                }
            }

            /* on bottom */
            if ((tmp.top() - selected.bottom()) > 0) {
                if (bottom == -1) {
                    bottom = i;
                    dist_bottom = tmp.top() - selected.bottom();
                } else if ((tmp.top() - selected.bottom()) < dist_bottom) {
                    bottom = i;
                    dist_bottom = tmp.top() - selected.bottom();
                }
            }
        }
    }

    for (int i = (crossed->size()-1); i >= 0; i--) {
        if (i != left && i != top && i != right && i != bottom)
            crossed->erase(crossed->begin() + i);
    }
}

/* Adjust a position on the nearest grid's square */
QPoint BloomPanelDesktopGrid::put_on_grid(QPoint point)
{
    return square_to_px(px_to_square(point));
}

/* Adjust a size on a round numbers of squares */
QSize BloomPanelDesktopGrid::put_on_grid(QSize square)
{
    return square_to_px(px_to_square(square));
}

/* Change a point from px to grid's squares */
QPoint BloomPanelDesktopGrid::px_to_square(QPoint point_px)
{
    int x, y;
    if (point_px.x() % grid_square_width < grid_square_width / 2)
        x = point_px.x() / grid_square_width;
    else
        x = (point_px.x() / grid_square_width) + 1;

    if (point_px.y() % grid_square_height < grid_square_height / 2)
        y = point_px.y() / grid_square_height;
    else
        y = (point_px.y() / grid_square_height) + 1;

    return QPoint(x, y);
}

/* Change a size from px to grid's squares */
QSize BloomPanelDesktopGrid::px_to_square(QSize size_px)
{
    int cx, cy;
    if (size_px.width() % grid_square_width < grid_square_width / 2)
        cx = size_px.width() / grid_square_width;
    else
        cx = (size_px.width() / grid_square_width) + 1;

    if (size_px.height() % grid_square_height < grid_square_height / 2)
        cy = size_px.height() / grid_square_height;
    else
        cy = (size_px.height() / grid_square_height) + 1;

    return QSize(cx, cy);
}

/* Resize the grid (resize the base square's size) */
void BloomPanelDesktopGrid::resize_grid(const QSize size)
{
    QSize new_size(size.width() / get_width() * get_width(),
                   size.height() / get_height() * get_height());

    if (!(new_size.height() / GRID_SIZE_Y) ||
        !(new_size.width() / GRID_SIZE_X))
        return;

    grid_square_height = new_size.height() / GRID_SIZE_Y;
    grid_square_width = new_size.width() / GRID_SIZE_X;

    BloomWidgetContainer* widget = NULL;
    for(int i = 0; i < desktop->widgets.size(); i++) {
        widget = desktop->widgets.at(i);
        widget->update_position();
        widget->update_size();
    }
}

/* Change a size from grid's squares to px */
QSize BloomPanelDesktopGrid::square_to_px(QSize size_square)
{
    QSize size_px = QSize();

    size_px.setWidth(size_square.width() * grid_square_width);
    size_px.setHeight(size_square.height() * grid_square_height);

    return size_px;
}

/* Change a size from grid's squares to px */
QPoint BloomPanelDesktopGrid::square_to_px(QPoint point_square)
{
    QPoint point_px = QPoint();

    point_px.setX(point_square.x() * grid_square_width);
    point_px.setY(point_square.y() * grid_square_height);

    return point_px;
}

