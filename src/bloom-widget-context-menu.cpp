/*
 * bloom-widget-context-menu
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: ROGER Josselin
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../config.h"

#include <locale.h>
#include <glib/gi18n.h>

#include "bloom-widget-context-menu.h"
#include "bloom-widget-container.h"

#define _(String) gettext (String)

BloomWidgetContextMenu::BloomWidgetContextMenu(BloomWidgetContainer *container)
    : QMenu(container)
{
    widget = container;
    left_button_pressed = false;

    QAction *action;
    if (bool(container->deletable)) {
        action = new QAction(_("&Delete this widget"), this);
        action->setStatusTip(_("Delete this widget"));
        addAction(action);
        connect(action, SIGNAL(triggered()), widget, SLOT(remove()));
    }

    QMenu *style_menu = addMenu(_("Style"));
    QStringList styles = container->get_available_styles();
    for (int i = 0; i < styles.size(); i++) {
        QAction *action = new QAction(styles[i], this);
        action->setData(QVariant(styles[i]));
        style_menu->addAction(action);
        connect(this, SIGNAL(triggered(QAction*)), this, SLOT (changed_style(QAction*)));
    }
}

void BloomWidgetContextMenu::show_menu()
{
    QPoint p = widget->mapToGlobal(widget->get_options_button()->pos());
    move(p.x(), p.y());
    show();
}

void BloomWidgetContextMenu::show_menu(QPoint point)
{
    move(point.x(), point.y());
    show();
}

void BloomWidgetContextMenu::mousePressEvent(QMouseEvent *event)
{
    if (!geometry().contains(QCursor::pos())) {
        widget->mousePressEvent(
            new QMouseEvent(event->type(),
                            widget->mapFromGlobal(QCursor::pos()),
                            event->button(),
                            event->buttons(),
                            event->modifiers()));
        event->ignore();
    } else if (QApplication::mouseButtons() == Qt::LeftButton) {
        left_button_pressed = true;
    }
}

void BloomWidgetContextMenu::mouseReleaseEvent(QMouseEvent *event)
{
    if (left_button_pressed) {
        hide();
        left_button_pressed = false;
        if (actionAt(mapFromGlobal(QCursor::pos())) != 0)
            emit actionAt(mapFromGlobal(QCursor::pos()))->trigger();
    }
}

void BloomWidgetContextMenu::changed_style(QAction *action)
{
    widget->style = action->data().toString();
    widget->changed_style();
}

