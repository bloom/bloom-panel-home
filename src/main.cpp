/*
 * main
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: ROGER Josselin
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtGui>

#define __GTK_BINDINGS_H__
#include <glib/gi18n.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

#ifndef NO_BLOOM_PANEL
#include <moblin-panel/mpl-panel-common.h>
#include <moblin-panel/mpl-panel-qt.h>
#include <libbloom-toolbar.h>
#else
#define TOOLBAR_HEIGHT 300
#endif

#include <QApplication>

#include <glib-object.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include "../config.h"

#include <locale.h>
#define _(String) gettext (String)

#include "bloom-panel-home.h"
#include "bloom-panel-home-dbus-adaptor.h"

BloomPanelHome *panel = NULL;
MplPanelClient *panel_client;

void set_window_size (BloomPanelHome*, int, int);

int main(int argc, char *argv[])
{
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    g_set_prgname("bloom-panel-home");

    gtk_init(&argc, &argv);
    QApplication app(argc, argv);

    gboolean standalone = FALSE;
    GError *error = NULL;
    GOptionEntry entries[] = {
         { "standalone", 's', 0, G_OPTION_ARG_NONE, &standalone, _("Run in standalone mode"), NULL }, { NULL }
    };

    GOptionContext *context;
    context = g_option_context_new("- bloom panel home");
    g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
    g_option_context_add_group (context, gtk_get_option_group (TRUE));
    if (!g_option_context_parse (context, &argc, &argv, &error)) {
        g_print ("option parsing failed: %s\n", error->message);
        exit (1);
    }

    QApplication::setStyle(new QPlastiqueStyle);
    gtk_settings_set_string_property(gtk_settings_get_default (),
                                     "gtk-theme-name",
                                     "bloom-panel-home",
                                     NULL);

#ifndef NO_BLOOM_PANEL
    if (!standalone) {
        panel_client = mpl_panel_qt_new(MPL_PANEL_MYZONE,
                                        _("Home"),
                                        THEMEDIR"/default/myzone-button.css",
                                        "unknown",
                                        TRUE);

        panel = new BloomPanelHome();
        mpl_panel_client_request_button_style(panel_client, "myzone");
        mpl_panel_qt_set_child(panel_client, panel);
        panel->set_panel_client(panel_client);
    }
    else
#endif
    {
        panel = new BloomPanelHome();
    }

    // Register on the message bus
    QString object_path;
    QDBusConnection connection = QDBusConnection::sessionBus();
    new BloomPanelHomeAdaptor(panel);

    bool ret = connection.registerService("org.agorabox.Bloom.Panel.Home");
    ret = connection.registerObject("/Home", panel);
    QApplication::processEvents();

    // Widgets are normally loaded the first time
    // the panel is displayed
    // panel->load_widgets();

    return app.exec();
}

