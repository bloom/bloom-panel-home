#ifndef BLOOM_PANEL_DESKTOP_GRID_H_
#define BLOOM_PANEL_DESKTOP_GRID_H_

/* Grid's size */
#define GRID_SIZE_X 40
#define GRID_SIZE_Y 30

#include <QtGui>
#include <vector>

class BloomWidgetContainer;
class BloomPanelDesktop;

using namespace std;

class BloomPanelDesktopGrid
{
    public:
        BloomPanelDesktopGrid                         (BloomPanelDesktop *parent = 0);
        bool              collision                   (BloomWidgetContainer *widget);
        bool              collision                   (QRect, BloomWidgetContainer *widget);
        void              find_nearest_place          (QRect*, QPoint);
        int               find_scope_of_positions     (BloomWidgetContainer *widget);
        vector<int>       get_crossed                 ()                      { return *crossed; }
        int               get_height                  ()                      { return GRID_SIZE_Y; }
        vector<QPoint>    get_scope_of_positions      ()                      { return *scope_of_positions; }
        QPoint            get_selected_origin         ()                      { return selected_origin; }
        int               get_width                   ()                      { return GRID_SIZE_X; }
        void              nearest_collision           (QRect);
        QPoint            put_on_grid                 (QPoint);
        QSize             put_on_grid                 (QSize);
        QPoint            px_to_square                (QPoint);
        QSize             px_to_square                (QSize);
        void              resize_grid                 (QSize);
        void              set_selected_origin         (QPoint selection)      { selected_origin = selection; }
        QPoint            square_to_px                (QPoint);
        QSize             square_to_px                (QSize);

    protected:
        int               nearest_divisor             (int, int);
        void              sort_by_area                (QRect[], int);

    private:
        BloomPanelDesktop *desktop;                   /* QWidget parent (bloom-panel-desktop) */
        vector<QPoint>    *scope_of_positions;        /* Positions of all bloom-widget on the grid */
        vector<int>       *crossed;                   /* Number of all widgets crossed by an dragged widget*/
        int               grid_square_height;         /* Number of square in the desktop's height */
        int               grid_square_width;          /* Number of square in the desktop's width */
        QPoint            selected_origin;            /* The origin position of the dragged bloom-widget */
};

#endif /* BLOOM_PANEL_DESKTOP_GRID_H_ */
