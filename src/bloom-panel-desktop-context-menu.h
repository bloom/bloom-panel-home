#ifndef HOME_PANEL_CONTEXT_MENU_H_
#define HOME_PANEL_CONTEXT_MENU_H_

#include <QtCore>
#include <QtGui>

class BloomPanelDesktop;

class BloomPanelDesktopContextMenu : public QMenu
{
    Q_OBJECT

    public:
        BloomPanelDesktopContextMenu(BloomPanelDesktop *parent);

    protected:
        void mousePressEvent            (QMouseEvent *event);
        void mouseReleaseEvent          (QMouseEvent *event);

    private slots:
        void show_menu                  (QPoint);

    private:
        BloomPanelDesktop *parent;      /* QWidget parent (bloom-panel-desktop) */
        bool left_button_pressed;       /* if the left-lick is clicked */
};

#endif /* HOME_PANEL_CONTEXT_MENU_H_ */
