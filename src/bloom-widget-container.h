#ifndef BLOOM_WIDGET_CONTAINER_H
#define BLOOM_WIDGET_CONTAINER_H

#include <QtCore>
#include <QtGui>
#include <QX11EmbedWidget>
#include <QtDBus/QtDBus>

#include "gconfitem.h"

#define MARGE 6
#define SHADOW_WIDTH 5
#define HEADER_CORNERS_WIDTH 9
#define HEADER_HEIGHT 25
#define BUTTON_WIDTH 18
#define BUTTON_HEIGHT 18

class BloomWidgetContextMenu;
class BloomPanelDesktopGrid;
class BloomWidgetProxy;
class BloomPanelDesktop;

// There are two options for a widget :
//  - either embed itself into the provided container
//  - either return a window and have it swallowed by the container

class BloomWidgetContainer : public QWidget
{
Q_OBJECT

public:
    BloomWidgetContainer(const QString &service, const QString &interface, const QString &path,
                         const QDBusConnection &connection, BloomPanelDesktop *desktop,
                         QString gconf_prefix, bool embed = true);
    virtual ~BloomWidgetContainer();

public:
    void          attach                  (BloomPanelDesktopGrid *grid);
    void          add_css_file            (QString, QString);
    void          embed_widget            ();

    void          init_embedded           ();
    QSize         get_area_size           ()                        { return background->size(); }
    QPoint        get_area_position       ()                        { return frame->pos(); }
    GConfItem&    get_gconf_entry         ()                        { return gconf_entry; }
    QWidget*      get_parent              ()                        { return (QWidget*) desktop; }
    QX11EmbedContainer*  get_embed_widget ()                        { return embed_client; }
    QPoint        get_position_on_grid    ()                        { return position_on_grid; }
    QSize         get_size_on_grid        ()                        { return size_on_grid; }
    QString       get_service_name        ()                        { return service; }
    std::string   get_side                ()                        { return side; }
    bool          is_fixed                ()                        { return fixed; }
    bool          is_on_the_grid          ()                        { return on_the_grid; }
    bool          is_vertical             ()                        { return vertical; }
    bool          is_embedded             ()                        { return embedded; }
    QPushButton*  get_delete_button       ()                        { return button_delete; }
    QPushButton*  get_options_button      ()                        { return button_options; }
    const QRect   rect                    ();
    void          set_allow_manual_resize (bool allow)              { allow_manual_resize = allow; }
    void          set_frame_border_from_style_sheet();
    QStringList   get_available_styles    ();


    void          set_minimum_size_on_grid (int, int);
    void          set_maximum_size_on_grid (int, int);
    void          set_on_the_grid          (bool is_on, bool vert = true, int thickness = -1);
    void          set_side                 (std::string side_)       { side = side_; }
    void          set_title                (QString);
    void          set_vertical             (bool vert)               { vertical = vert; }

    void          init                            (QWidget*, QPoint, QSize, bool, bool, int, bool, bool);
    QSize         ensure_inside                   (QPoint point);

    void          set_background                  (QImage, bool);
    void          change_background               (QImage img, bool auto_resize = true);
    void          change_background               (QPixmap pix, bool auto_resize = true);

    void          show_caption                    (bool state);
    void          update_shadow                   ();
    void          move_square_by_square           (QPoint);
    void          resize_bottom                   (QPoint);
    void          resize_bottom_left              (QPoint, QPoint);
    void          resize_bottom_right             (QPoint);
    void          resize_left                     (QPoint);
    void          resize_right                    (QPoint);
    void          resize_top                      (QPoint);
    void          resize_top_left                 (QPoint);
    void          resize_top_right                (QPoint, QPoint);
    void          set_type                        (std::string);
    void          setMouseTrackingAllChildren     ();

    void          enterEvent                      (QEvent*);
    void          leaveEvent                      (QEvent*);
    void          resizeEvent                     (QResizeEvent*);
    void          moveEvent                       (QMoveEvent*);
    void          mouseMoveEvent                  (QMouseEvent*);
    void          mousePressEvent                 (QMouseEvent *event);
    void          mouseReleaseEvent               (QMouseEvent * event);
    void          contextMenuEvent                (QContextMenuEvent *event);
    void          timerEvent                      (QTimerEvent *event);

public slots:
    void          update_position         ();
    void          update_size             ();
    void          changed_visibility      ();
    void          changed_style           ();
    void          changed_title           ();
    void          changed_alpha           ();
    void          changed_favicon         ();
    void          toggle_shadow           ();
    void          remove                  ();
    void          close                   ();
    void          widget_removed          ();
    void          client_closed           ();
    void          client_embedded         ();
    void          widget_created          (QDBusPendingCallWatcher*);
    void          embedding_failed        (QX11EmbedContainer::Error error);

private:
    QBoxLayout                    *main_layout;
    BloomWidgetContextMenu        *menu;
    BloomWidgetProxy              *proxy;
    QX11EmbedContainer            *embed_client;
    QWidget                       *shadow;
    BloomPanelDesktopGrid         *grid;
    BloomPanelDesktop             *desktop;

    QWidget                       *background;
    QPushButton                   *button_delete;
    QPushButton                   *button_options;
    QFrame                        *frame;
    QWidget                       *header_left;
    QWidget                       *header;
    QWidget                       *header_right;
    QLabel                        *title_label;
    QLabel                        *favicon_label;
    QLabel                        *wait_animation;
    QMovie                        *wait_movie;
    QIcon                         *favicon_icon;
    QList<QWidget*>               auto_resize_widgets;
    QDBusPendingCallWatcher       *create_call_watcher;

    bool                          allow_manual_resize;
    int                           background_auto_resize;
    QImage                        background_img;
    QPixmap                       background_pix;
    int                           frame_border_bottom;
    int                           frame_border_left;
    int                           frame_border_right;
    int                           frame_border_top;
    int                           gconf_number;
    bool                          manual_resize;
    bool                          remove_gconf;
    int                           number;
    bool                          on_the_grid;
    QList<QSize>                  original_size;
    QPalette                      palette;
    int                           position_cursor;
    std::string                   side;
    bool                          vertical;
    int                           winid;
    bool                          embedded;
    QString                       service;

public:
    GConfItem                     gconf_entry;
    GConfItem                     schema;
    GConfItem                     type;
    GConfItem                     fixed;
    GConfItem                     visible;
    GConfItem                     title;
    GConfItem                     favicon;
    GConfItem                     position_on_grid;
    GConfItem                     size_on_grid;
    GConfItem                     thickness;
    GConfItem                     margin;
    GConfItem                     style;
    GConfItem                     alpha;
    GConfItem                     show_shadow;
    GConfItem                     minimum_size_on_grid;
    GConfItem                     maximum_size_on_grid;
    GConfItem                     deletable;
    GConfItem                     has_options;
    GConfItem                     auto_hide_caption;
    GConfItem                     deleted;
};

#endif

