#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include "bloom-panel-desktop.h"
#include "bloom-panel-home.h"

/*
 * bloom-panel-desktop
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: Sylvain BAUBEAU
 *         Josselin ROGER
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*******************************
 *      Public Methodes        *
 *******************************/

BloomPanelDesktop::BloomPanelDesktop(QWidget *parent) :
    QWidget(parent),
    background_url("/desktop/gnome/background/picture_filename"),
    background_options("/desktop/gnome/background/picture_options")
{
    /* Initialisations */
    drop_in_progress = false;
    highlighted_rect = QRect();
    is_out = true;
    this->parent = (BloomPanelHome*) parent;
    highlighted_widget = new QWidget(this);
    highlighted_widget->setObjectName("highlightedWidget");
    highlighted_widget->setAcceptDrops(false);
    selected_widget = NULL;

    /* Initializes the grid */
    grid = new BloomPanelDesktopGrid(this);

    /* set the base size of the desktop */
    // setBaseSize(10 * grid->get_width(), 10 * grid->get_height());

    /* resize proportionally in grid squares */
    // setSizeIncrement(grid->get_width(), grid->get_height());

    setAcceptDrops(true);
    setMouseTracking(true);

    load();

    setAutoFillBackground(false);
    // setAttribute(Qt::WA_NoBackground, true);
    setAttribute(Qt::WA_NoSystemBackground, true);

    QObject::connect(&background_url, SIGNAL(valueChanged()), this, SLOT(load_background()));

    emit load_background();
}

/******************************
 *      Protected Methods     *
 ******************************/

/* Executed action on mouse press event */
void BloomPanelDesktop::mousePressEvent(QMouseEvent *event)
{
    /* hide the contextual menu */
    menu->hide();

    if(QApplication::mouseButtons() == Qt::LeftButton) {
        /*
         * On left click mouse event
         */
        if(widgets.size() > 0) {
            int i = -1;
            /*
            * find if the mouse is on a bloom-widget
            * In this case, apply a drag on the hovering bloom-widget
            */
            BloomWidgetContainer *widget;
            do {
                i++;
                widget = widgets.at(i);
                if (!widget->is_fixed() && widget->is_on_the_grid() &&
                    widget->frameGeometry().contains(event->pos())) {

                    /* Save the selected bloom-widget's number and origin position */
                    selected_widget = widget;
                    grid->set_selected_origin(widget->pos());
                    /* bring the selected bloom-widget to the foreground */
                    widget->raise();

                    /* Initialize the drag */
                    QByteArray itemData;
                    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
                    dataStream << i;

                    QMimeData *mimeData = new QMimeData;
                    mimeData->setData("application/x-bloom-widget", itemData);

                    selected_widget = widget;
                    QDrag *drag = new QDrag(widget);

                    drag->setMimeData(mimeData);
                    position_on_widget = event->pos() - widget->pos();
                    drag->setHotSpot(position_on_widget);

                    /* Find all position where the bloom-widget can be drop */
                    grid->find_scope_of_positions(widget);

                    /* Allow/Launch the drag move action */
                    // if (!(drag->exec(Qt::MoveAction) == Qt::MoveAction)) {}
                    // if (!(drag->exec(0) == 0)) {}
                    highlighted_rect = widget->rect();
                    highlighted_widget->setGeometry(highlighted_rect);

                    grabMouse();
                    is_out = false;

                    /* change mouse cursor - useless ??? */
                    QApplication::setOverrideCursor(Qt::ClosedHandCursor);
                    drag->setDragCursor(QApplication::overrideCursor()->pixmap(), Qt::CopyAction);
                    drag->setDragCursor(QApplication::overrideCursor()->pixmap(), Qt::MoveAction);
                    drag->setDragCursor(QApplication::overrideCursor()->pixmap(), Qt::LinkAction);
                }
            } while (i < widgets.size() - 1 && widget != selected_widget);
        }
    } else if (QApplication::mouseButtons() == Qt::RightButton) {
        /*
         * On right click mouse event
         */

        /* find if the mouse cursor is on a BloomWidgetContainer */
        int i = 0;
        bool is_intersected = false;
        while (i < widgets.size() && !is_intersected) {
            if (widgets.at(i)->is_on_the_grid())
                is_intersected = widgets.at(i)->rect().contains(mapFromGlobal(QCursor::pos()));
            i++;
        }

        if (is_intersected) {
            /* Transfer the event to the BloomWidgetContainer's mousePressEvent (if event is a show_menu event) */
            widgets.at(i-1)->mousePressEvent(event);
        } else if (geometry().contains(QCursor::pos())) {
            /* open the home_panel contextuel menu */
            emit customContextMenuRequested(QCursor::pos());
            event->accept();
        }
    }
}

void BloomPanelDesktop::load_background()
{
    if (!background.load(background_url)) {
         background = QImage(size(), QImage::Format_Mono);
         background.fill(Qt::black);
    }
    else {
         if (QString(background_options) == "stretched") {
             background = background.scaled(size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
         }
    }
    update();
}

 /* Load elements on the desktop (and restore the desktop from the gconf) */
void BloomPanelDesktop::load()
{
    /* create the right-click menu */
    menu = new BloomPanelDesktopContextMenu(this);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), menu, SLOT(show_menu(QPoint)));
}

void BloomPanelDesktop::mouseMoveEvent(QMouseEvent *event)
{
    event->accept();
    /* if the cursor is in the desktop area & the grid knows the dragged widget's number */
    if (!is_out && selected_widget && event->buttons() & Qt::LeftButton) {
        grid->find_nearest_place(&highlighted_rect, selected_widget->rect().topLeft());
        if (highlighted_widget->rect() != highlighted_rect)
            highlighted_widget->setGeometry(highlighted_rect);

        /* move the dragged widget on the desktop */
        QPoint new_pos = event->pos() - position_on_widget;
        if(new_pos.x() < 0)
            new_pos.setX(0);
        if(new_pos.y() < 0)
            new_pos.setY(0);
        if(new_pos.x() > (width() - selected_widget->width()))
            new_pos.setX(width() - selected_widget->width());
        if(new_pos.y() > (height() - selected_widget->height()))
            new_pos.setY(height() - selected_widget->height());

        selected_widget->move(new_pos);
    }
}

void BloomPanelDesktop::mouseReleaseEvent(QMouseEvent *event)
{
    if (!selected_widget)
        return;

    releaseMouse();
    drop_in_progress = true;

    /*
     * If the mouse is outside the desktop
     * Place the dragged object at the highlighted position
     */
    QPoint local_cursor_position = mapFromGlobal(event->pos());
    if (local_cursor_position.x() < 0) {
        selected_widget->move(0, highlighted_rect.top());
    } else if (local_cursor_position.x() > width()) {
        selected_widget->move(
            width() - selected_widget->width(),
            highlighted_rect.top());
    }
    if (local_cursor_position.y() < 0) {
        selected_widget->move(highlighted_rect.left(), 0);
    } else if (local_cursor_position.y() > height()) {
        selected_widget->move(
            highlighted_rect.left(), height() - selected_widget->height());
    }

    /* If the dragged bloom-widget cross another bloom-widget */
    if(grid->collision(selected_widget)) {
        is_out = true;
        /* Place the highlighted at the nearest free space */
        grid->find_nearest_place(&highlighted_rect, selected_widget->rect().topLeft());

        /* Place the dragged bloom-widget at the highlighted position */
        if (QApplication::mouseButtons() != Qt::LeftButton ||
            (QApplication::mouseButtons() == Qt::LeftButton &&
              (local_cursor_position.x() > width()-1 ||
               local_cursor_position.x() < 0 ||
               local_cursor_position.y() > height()-1 ||
               local_cursor_position.y() < 0))) {
            selected_widget->move(highlighted_rect.topLeft());
        }
    }

    if (highlighted_rect.isValid())
        selected_widget->move(highlighted_rect.topLeft());

    if (!selected_widget->is_embedded())
        selected_widget->init_embedded();

    drop_in_progress = false;
    event->ignore();

    highlighted_rect = QRect();
    selected_widget = NULL;
    update();
}

void BloomPanelDesktop::moveEvent(QMoveEvent*)
{
    for (int i = 0;i < widgets.size(); i++) {
        widgets[i]->update_shadow();
    }
}

/* Paint desktop elements */
void BloomPanelDesktop::paintEvent(QPaintEvent *event)
{
    QPainter p(this);

    p.drawImage(background_pos, background);

    /* Paint highlighted if exist */
    if (highlighted_rect.isValid()) {
        highlighted_widget->show();
        highlighted_widget->lower();
    } else if (highlighted_widget->isVisible()){
        highlighted_widget->hide();
    }

    event->accept();
    // QWidget::paintEvent(event);
}

/* Executed action on resize event */
void BloomPanelDesktop::resizeEvent(QResizeEvent* event)
{
    emit load_background();

    QWidget::resizeEvent(event);

    /* resize the grid */
    grid->resize_grid(event->size());
    
    event->accept();

    /* update the highlighted */
    highlighted_rect = QRect();
}

/*******************************
 *        Private Slots        *
 *******************************/

/* Change the background */
void BloomPanelDesktop::choose_background()
{
    QProcess *process = new QProcess();
    QStringList arguments;
    arguments.append("-p");
    arguments.append("background");
    process->start("gnome-appearance-properties", arguments);
    return;
}

/* Create or put a new bloom-widget on the desktop */
BloomWidgetContainer* BloomPanelDesktop::add_widget(BloomWidgetContainer *new_widget)
{
    QPoint pos(0, 0);
    if (!drop_in_progress) {
        if (new_widget->is_on_the_grid()) {
            /* put the new bloom-widget on the indicated position
             * or, without indication, on the cursor's position
             */

            /* if the new bloom-widget can be put on the desktop */
            if(grid->find_scope_of_positions(new_widget) == 0) {
                qDebug() << "No free room for widget";
                new_widget->remove();
                return NULL;
            }
            else {
                widgets.append(new_widget);
            }
        }
    }
    return new_widget;
}

void BloomPanelDesktop::remove_widget(BloomWidgetContainer *container)
{
    drop_in_progress = true;
    widgets.removeOne(container);
    highlighted_rect = QRect();
    drop_in_progress = false;
    delete container;
}

void BloomPanelDesktop::relayout()
{
    margin_left = 0;
    margin_right = width();
    margin_top = 0;
    margin_bottom = height();

    BloomWidgetContainer* tmp = NULL;
    for(int i = 0; i < widgets.size(); i++) {
        tmp = widgets.at(i);
        if (!tmp->is_on_the_grid()) {
            if (tmp->is_vertical()) {
                if (tmp->get_side() == "right") {
                    tmp->move(margin_right - tmp->width(), 0);
                    margin_right = tmp->pos().x();
                } else {
                    tmp->move(margin_left, 0);
                    margin_left = tmp->pos().x() + tmp->width();
                }
            } else {
                if (tmp->get_side() == "bottom") {
                    tmp->move(0, margin_bottom - tmp->height());
                    margin_bottom = tmp->pos().y();
                } else {
                    tmp->move(0, margin_top);
                    margin_top = tmp->pos().y() + tmp->height();
                }
            }
        }
    }
    background_pos = QPoint(0, 0);
    background_size = size();
}

