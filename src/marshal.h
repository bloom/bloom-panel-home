
#ifndef __moblin_netbook_marshal_MARSHAL_H__
#define __moblin_netbook_marshal_MARSHAL_H__

#include    <glib-object.h>

G_BEGIN_DECLS

/* VOID:ENUM (./marshal.list:1) */
#define moblin_netbook_marshal_VOID__ENUM    g_cclosure_marshal_VOID__ENUM

/* VOID:INT64 (./marshal.list:2) */
extern void moblin_netbook_marshal_VOID__INT64 (GClosure     *closure,
                                                GValue       *return_value,
                                                guint         n_param_values,
                                                const GValue *param_values,
                                                gpointer      invocation_hint,
                                                gpointer      marshal_data);

/* VOID:STRING (./marshal.list:3) */
#define moblin_netbook_marshal_VOID__STRING  g_cclosure_marshal_VOID__STRING

/* VOID:UINT,UINT (./marshal.list:4) */
extern void moblin_netbook_marshal_VOID__UINT_UINT (GClosure     *closure,
                                                    GValue       *return_value,
                                                    guint         n_param_values,
                                                    const GValue *param_values,
                                                    gpointer      invocation_hint,
                                                    gpointer      marshal_data);

/* VOID:INT,INT (./marshal.list:5) */
extern void moblin_netbook_marshal_VOID__INT_INT (GClosure     *closure,
                                                  GValue       *return_value,
                                                  guint         n_param_values,
                                                  const GValue *param_values,
                                                  gpointer      invocation_hint,
                                                  gpointer      marshal_data);

/* VOID:VOID (./marshal.list:6) */
#define moblin_netbook_marshal_VOID__VOID    g_cclosure_marshal_VOID__VOID

/* VOID:STRING,INT,BOOLEAN (./marshal.list:7) */
extern void moblin_netbook_marshal_VOID__STRING_INT_BOOLEAN (GClosure     *closure,
                                                             GValue       *return_value,
                                                             guint         n_param_values,
                                                             const GValue *param_values,
                                                             gpointer      invocation_hint,
                                                             gpointer      marshal_data);

/* VOID:OBJECT,UINT (./marshal.list:8) */
extern void moblin_netbook_marshal_VOID__OBJECT_UINT (GClosure     *closure,
                                                      GValue       *return_value,
                                                      guint         n_param_values,
                                                      const GValue *param_values,
                                                      gpointer      invocation_hint,
                                                      gpointer      marshal_data);

/* BOOLEAN:VOID (./marshal.list:9) */
extern void moblin_netbook_marshal_BOOLEAN__VOID (GClosure     *closure,
                                                  GValue       *return_value,
                                                  guint         n_param_values,
                                                  const GValue *param_values,
                                                  gpointer      invocation_hint,
                                                  gpointer      marshal_data);

G_END_DECLS

#endif /* __moblin_netbook_marshal_MARSHAL_H__ */

